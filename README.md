To jest puste repozytorium dla projektu '2016 - L04 - G1 System do zarządzania zadaniami'.

Żeby je sklonować, użyj:

    git clone http://uzytkownik@rumcajs.prz-rzeszow.pl/git/l04-g1-system-do-zarzadzania-zadaniami

- w URL zamiast słowa `uzytkownik` wpisz swoją nazwę użytkownika z Redmine,
- przy próbie dostępu podaj swoje hasło do Redmine.

Tego samego URL możesz używać w innych operacjach (`pull`, `push` itp).

