﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace System_zarzadania_zadaniami.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestCase("1995-01-01", true)]
        [TestCase("1994-01-01", true)]
        [TestCase("2000-01-01", true)]
        [TestCase("2001-01-01", false)]
        [TestCase("2005-01-01", false)]
        public void Sprawdz_CzyDorosly_Osoba(DateTime s, bool spodziewane)
        {
            var win = new Osoba();
            bool wynik = win.Czy_dorosly(s);

            Assert.AreEqual(spodziewane, wynik);
        }
    }
}
