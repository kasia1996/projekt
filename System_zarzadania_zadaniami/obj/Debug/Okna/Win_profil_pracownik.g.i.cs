﻿#pragma checksum "..\..\..\Okna\Win_profil_pracownik.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "CDBC8322999F234663BDF5650375719657E220B3"
//------------------------------------------------------------------------------
// <auto-generated>
//     Ten kod został wygenerowany przez narzędzie.
//     Wersja wykonawcza:4.0.30319.42000
//
//     Zmiany w tym pliku mogą spowodować nieprawidłowe zachowanie i zostaną utracone, jeśli
//     kod zostanie ponownie wygenerowany.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using System_zarzadania_zadaniami;
using System_zarzadania_zadaniami.Okna;


namespace System_zarzadania_zadaniami.Okna {
    
    
    /// <summary>
    /// Win_profil_pracownik
    /// </summary>
    public partial class Win_profil_pracownik : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 17 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.ScaleTransform skala;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabControl Tab;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label1;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label2;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label3;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label4;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label5;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label6;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label7;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label ImieLabel;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label NazwiskoLabel;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label HasloLabel;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label DataUrodzeniaLabel;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label DataZatrudnieniaLabel;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label KierownikLabel;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label StanowiskoLabel;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid ocenyDataGrid;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn lista_Zadan_ID_listyColumn;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn Kierownik;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn ocenaColumn;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn typ_Oceny_ID_typu_ocenyColumn;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn notkaColumn1;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GridSplitter gridSplitter;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid Urlop_DataGrid;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTemplateColumn data_rozpoczeciaColumn;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTemplateColumn data_zakonczeniaColumn;
        
        #line default
        #line hidden
        
        
        #line 84 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn typ_Urlopu_ID_typu_urlopuColumn;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn notkaColumn;
        
        #line default
        #line hidden
        
        
        #line 89 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker Data_zak;
        
        #line default
        #line hidden
        
        
        #line 99 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox UrlopNotatka;
        
        #line default
        #line hidden
        
        
        #line 100 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox ComboTypUrlopu;
        
        #line default
        #line hidden
        
        
        #line 101 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker Data_rozp;
        
        #line default
        #line hidden
        
        
        #line 111 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label;
        
        #line default
        #line hidden
        
        
        #line 112 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy;
        
        #line default
        #line hidden
        
        
        #line 113 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button button;
        
        #line default
        #line hidden
        
        
        #line 114 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy1;
        
        #line default
        #line hidden
        
        
        #line 119 "..\..\..\Okna\Win_profil_pracownik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Text_tab;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/System_zarzadania_zadaniami;component/okna/win_profil_pracownik.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Okna\Win_profil_pracownik.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 9 "..\..\..\Okna\Win_profil_pracownik.xaml"
            ((System_zarzadania_zadaniami.Okna.Win_profil_pracownik)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            
            #line 9 "..\..\..\Okna\Win_profil_pracownik.xaml"
            ((System_zarzadania_zadaniami.Okna.Win_profil_pracownik)(target)).SizeChanged += new System.Windows.SizeChangedEventHandler(this.Window_SizeChanged);
            
            #line default
            #line hidden
            
            #line 9 "..\..\..\Okna\Win_profil_pracownik.xaml"
            ((System_zarzadania_zadaniami.Okna.Win_profil_pracownik)(target)).StateChanged += new System.EventHandler(this.Window_StateChanged);
            
            #line default
            #line hidden
            return;
            case 2:
            this.skala = ((System.Windows.Media.ScaleTransform)(target));
            return;
            case 3:
            this.Tab = ((System.Windows.Controls.TabControl)(target));
            
            #line 19 "..\..\..\Okna\Win_profil_pracownik.xaml"
            this.Tab.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.Tab_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 4:
            this.label1 = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.label2 = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.label3 = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.label4 = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.label5 = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.label6 = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.label7 = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.ImieLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.NazwiskoLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.HasloLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 14:
            this.DataUrodzeniaLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 15:
            this.DataZatrudnieniaLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 16:
            this.KierownikLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 17:
            this.StanowiskoLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 18:
            this.ocenyDataGrid = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 19:
            this.lista_Zadan_ID_listyColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 20:
            this.Kierownik = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 21:
            this.ocenaColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 22:
            this.typ_Oceny_ID_typu_ocenyColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 23:
            this.notkaColumn1 = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 24:
            this.gridSplitter = ((System.Windows.Controls.GridSplitter)(target));
            return;
            case 25:
            this.Urlop_DataGrid = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 26:
            this.data_rozpoczeciaColumn = ((System.Windows.Controls.DataGridTemplateColumn)(target));
            return;
            case 27:
            this.data_zakonczeniaColumn = ((System.Windows.Controls.DataGridTemplateColumn)(target));
            return;
            case 28:
            this.typ_Urlopu_ID_typu_urlopuColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 29:
            this.notkaColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 30:
            this.Data_zak = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 31:
            this.UrlopNotatka = ((System.Windows.Controls.TextBox)(target));
            return;
            case 32:
            this.ComboTypUrlopu = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 33:
            this.Data_rozp = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 34:
            this.label = ((System.Windows.Controls.Label)(target));
            return;
            case 35:
            this.label_Copy = ((System.Windows.Controls.Label)(target));
            return;
            case 36:
            this.button = ((System.Windows.Controls.Button)(target));
            
            #line 113 "..\..\..\Okna\Win_profil_pracownik.xaml"
            this.button.Click += new System.Windows.RoutedEventHandler(this.button_Click);
            
            #line default
            #line hidden
            return;
            case 37:
            this.label_Copy1 = ((System.Windows.Controls.Label)(target));
            return;
            case 38:
            this.Text_tab = ((System.Windows.Controls.TextBlock)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

