﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace System_zarzadania_zadaniami.Okna
{
    /// <summary>
    /// Interaction logic for Win_raport_do_admina.xaml
    /// </summary>
    public partial class Win_raport_do_admina : Window
    {
        private int ID_pracownika = Win_logowanie.IdPracownika;

        public Win_raport_do_admina()
        {
            InitializeComponent();
            LadowanieRaporty();
        }
        private void LadowanieRaporty()
        {
            using (var db = new BazaEntities())
            {
                var wyszukaj = from p in db.Zadanie where p.Pracownicy_ID_pracownika == ID_pracownika select p;
                var wyszukaj2 = from p in db.Raport
                                where p.Pracownicy_ID_pracownika == ID_pracownika
                                select p.Zadanie_ID_zadania;
                var dane = wyszukaj2.ToList();
                foreach (var kier in wyszukaj)
                {
                    if (!dane.Exists(x => x.ToString() == kier.ID_zadania.ToString()))
                        RaportcomboBox.Items.Add(kier.ID_zadania);
                }
            }
        }

        //wczytanie opisu
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if (RaportcomboBox.SelectedIndex>-1)
            {
                using (var db = new BazaEntities())
                {
                    var IDzad = Convert.ToDecimal(RaportcomboBox.Text);
                    var wyszukaj = from p in db.Zadanie where p.ID_zadania == IDzad select p;
                    var wyszukajka = wyszukaj.FirstOrDefault<Zadanie>();
                    opisBox.Text = wyszukajka.Opis;
                }
            }
        }

        //wyslanie raportu
        private void Raportbutton1_Click(object sender, RoutedEventArgs e)
        {
            if (RaportcomboBox.Text == "" || RaporttextBox.Text == "")
                MessageBox.Show("Należy uzupełnić wszystkie dane do rapotu (ID Zadania i treść raportu) !");
            else
            {
                using (var db = new BazaEntities())
                {
                    var wyciag1 = from p in db.Raport select p.ID_raportu;
                    var listaID = wyciag1.ToList();
                    var id1 = Convert.ToDecimal(ID_pracownika);
                    var id2 = Convert.ToDecimal(RaportcomboBox.Text);


                    Raport nowyRaport;
                    nowyRaport = new Raport
                    {
                        ID_raportu = listaID.Last() + 1,
                        Data_utworzenia = DateTime.Now,
                        Opis = RaporttextBox.Text,
                        Pracownicy_ID_pracownika = id1,
                        Zadanie_ID_zadania = id2
                    };
                    db.Raport.Add(nowyRaport);
                    try
                    {
                        db.SaveChanges();
                        MessageBox.Show("Pomyślnie przesłano raport");
                        RaporttextBox.Text = "";
                        opisBox.Text = "";
                    }
                    catch (Exception)
                    {
                        db.Dispose();
                        MessageBox.Show("Taki raport już istnieje");
                    }
                }
            }
        }

        //wybor zadania
        private void RaportcomboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            using (var db = new BazaEntities())
            {
                var IDzad = Convert.ToDecimal((sender as ComboBox).SelectedItem);
                var wyszukaj = from p in db.Zadanie where p.ID_zadania == IDzad select p;
                var wyszukajka = wyszukaj.FirstOrDefault<Zadanie>();
                opisBox.Text = wyszukajka.Opis;
            }
        }

    }
}
