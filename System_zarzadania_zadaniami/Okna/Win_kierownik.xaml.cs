﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Objects;
using System.Data.EntityClient;
using System.Data;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System_zarzadania_zadaniami.Okna;

namespace System_zarzadania_zadaniami
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow2 : Window
    {
        Thread _watek_czatu;
        private bool flaga = true;
        private bool flaga2;
        Stopwatch _zegar_watku_czatu = new Stopwatch();
        private List<Czat> _dane_czatu;
        private List<Urlop> _dane_Urlop;
        private Oceny getRow;


        private int ID_pracownika = Win_logowanie.IdPracownika;//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        public MainWindow2()
        {
            InitializeComponent();
            ID_pracownika = Win_logowanie.IdPracownika;
            Wiadomosc.MaxLength = 254;
            _watek_czatu = new Thread(Procedura_watku_czatu);
            _watek_czatu.Start();
            czatDataGrid.Visibility = Visibility.Hidden;
            Wiadomosc.Visibility = Visibility.Hidden;
            Wyslij.Visibility = Visibility.Hidden;

        }

        // odswiezenie czatu
        private void Odswiezenie_danych_Czatu()
        {
            using (var db = new BazaEntities())//laczymy sie z baza
            {
                var wyszukaj = from p in db.Czat orderby p.Data_wyslania select p;

                foreach (var dane in wyszukaj)
                {
                    var d = dane.Pracownicy.Imie;
                    d = dane.Pracownicy.Nazwisko;
                }
                var dane2 = wyszukaj.ToList();
                _dane_czatu = dane2;


                czatDataGrid.ItemsSource = Filtrowanie_Czatu(dane2);
            }
        }
        //Watek, ktory co pewien czas odswieza datagrid i dodaje nowe wiadomosci
        private void Procedura_watku_czatu()
        {
            while (flaga)
            {

                if (flaga2)
                {
                    using (var db = new BazaEntities())//laczymy sie z baza
                    {
                        IList wybrany = null;
                        zadanieDataGrid.Dispatcher.Invoke(new Action(() =>
                        {

                            wybrany = zadanieDataGrid.SelectedItems;
                        }));
                        var zadanie = (Zadanie)wybrany[0];
                        var ID_zadania = (int)zadanie.ID_zadania;

                        decimal wyszukaj = 0;
                        try
                        {
                            wyszukaj = (from p in db.Czat select p.ID_wiadomosci).Max();
                        }
                        catch (Exception)
                        {

                        }
                        Czat dodanie = null;
                        try
                        {
                            Wiadomosc.Dispatcher.Invoke(new Action(() =>
                            {
                                dodanie = new Czat()
                                {
                                    ID_wiadomosci = ((int)wyszukaj) + 1,
                                    Data_wyslania = DateTime.Now,
                                    Tresc = Wiadomosc.Text,
                                    Zadanie_ID_zadania = ID_zadania,
                                    Pracownicy_ID_pracownika = ID_pracownika,
                           
                                };
                                Wiadomosc.Text = "";
                            }));
                        }
                        catch (Exception)
                        {

                            //
                        }

                        db.Czat.Add(dodanie);

                        try
                        {
                            db.SaveChanges();
                        }
                        catch (Exception)
                        {
                            db.Dispose();

                        }
                        try
                        {
                            czatDataGrid.Dispatcher.Invoke(new Action(() =>
                            {
                                Odswiezenie_danych_Czatu();
                            }));
                            flaga2 = false;
                        }
                        catch (Exception)
                        {

                            //
                        }

                    }
                }


                if (_zegar_watku_czatu.Elapsed.Seconds == 10)
                {
                    _zegar_watku_czatu.Restart();
                    czatDataGrid.Dispatcher.Invoke(new Action(() =>
                    {
                        using (var db = new BazaEntities())//laczymy sie z baza
                        {
                            var wyszukaj = from p in db.Czat orderby p.Data_wyslania select p;

                            foreach (var dane in wyszukaj)
                            {
                                var d = dane.Pracownicy.Imie;
                                d = dane.Pracownicy.Nazwisko;
                            }
                            var dane2 = wyszukaj.ToList();
                            _dane_czatu = dane2;

                            czatDataGrid.ItemsSource = Filtrowanie_Czatu(dane2);
                        }
                    }));
                    ladowaie_danych_urlop();
                    ladowanie_zadan();
                }
            }
        }

        public ICollectionView Filtrowanie_Czatu(List<Czat> Czat)
        {
            if (zadanieDataGrid.SelectedIndex == -1) return null;
            IList wybrany = zadanieDataGrid.SelectedItems;
            var zadanie = (Zadanie)wybrany[0];
            var ID_zadania = (int)zadanie.ID_zadania;
            var filtr = new Predicate<object>(i => ((Czat)i).Zadanie_ID_zadania == ID_zadania);

            var dane2 = Czat;
            var itemSourceList = new CollectionViewSource() { Source = dane2 };
            var itemlist = itemSourceList.View;
            itemlist.Filter = filtr;

            return itemlist;
        }
        //metoda do wypisywania nazwy zakładki w ktorej sie znajdujemy
        private void Tab_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var _nazwa = Tab.SelectedItem;
            Text_tab.Text = ((TabItem)_nazwa).Header.ToString();
        }

        private void ladowanie_zadan()
        {
            using (var db = new BazaEntities())
            {

                var wyszukaj = from p in db.Zadanie where p.Pracownicy_ID_pracownika == ID_pracownika select p;

                foreach (var _dane in wyszukaj)
                {
                    var d = _dane.Status.Nazwa;
                    d = _dane.Typ_Zadania.nazwa;
                    d = _dane.Pracownicy.Nazwisko;
                    d = _dane.Pracownicy.Imie;


                }
                var dane = wyszukaj.ToList();
                zadanieDataGrid2.Dispatcher.Invoke(new Action(() =>
                {
                    zadanieDataGrid2.ItemsSource = dane;
                    zadanieDataGrid2.IsHitTestVisible = zadanieDataGrid2.HasItems;
                }));
            }
        }

        private void ladowanie_Ocen()
        {
            using (var db = new BazaEntities())
            {

                var wyszukaj = from p in db.Oceny
                                   // where p.Lista_Zadan.Zadanie.Pracownicy_ID_pracownika == ID_pracownika
                               select p;

                foreach (var _dane in wyszukaj)
                {
                    try
                    {
                        var d = _dane.Typ_Oceny.nazwa;
                        d = _dane.Lista_Zadan.Zadanie.Opis;
                        d = _dane.Lista_Zadan.Pracownicy.Nazwisko;
                        d = _dane.Lista_Zadan.Pracownicy.Imie;
                    }
                    catch (Exception)
                    {

                        //
                    }



                }

                var wyszukaj2 = from p in db.Typ_Oceny

                                select p.nazwa;

                foreach (var dane2 in wyszukaj2)
                {
                    ComboTypOceny.Items.Add(dane2);
                }


                var dane = wyszukaj.ToList();
                ocenyDataGrid.Dispatcher.Invoke(new Action(() =>
                {
                    ocenyDataGrid.ItemsSource = dane;
                    ocenyDataGrid.IsHitTestVisible = zadanieDataGrid2.HasItems;
                }));
            }
        }

        private void Main_Loaded(object sender, RoutedEventArgs e)
        {
            System.Windows.Data.CollectionViewSource ocenyViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("ocenyViewSource")));

            System.Windows.Data.CollectionViewSource zadanieViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("zadanieViewSource")));
            using (var db = new BazaEntities())//laczymy sie z baza
            {
                var wyszukaj = from p in db.Zadanie from c in db.Lista_Zadan where p.ID_zadania == c.Zadanie_ID_zadania && c.Pracownicy_ID_pracownika == ID_pracownika && p.Status_ID_statusu != 3 select p;

                var ccc = wyszukaj.ToList();
                foreach (var dane in wyszukaj)
                {
                    var d = dane.Pracownicy.Nazwisko;
                }

                zadanieDataGrid.ItemsSource = wyszukaj.ToList();
            }
            System.Windows.Data.CollectionViewSource czatViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("czatViewSource")));

            using (var db = new BazaEntities()) //laczymy sie z baza
            {
                var wyszukaj = from p in db.Czat orderby p.Data_wyslania select p;

                foreach (var dane in wyszukaj)
                {
                    var d = dane.Pracownicy.Imie;
                    d = dane.Pracownicy.Nazwisko;
                }
                var dane2 = wyszukaj.ToList();
                _dane_czatu = dane2;
            }
            _zegar_watku_czatu.Start();
            ladowanie_zadan();
            ladowaie_danych_urlop();
            ladowanie_Ocen();

        }

        private void ladowaie_danych_urlop()
        {
            using (var db = new BazaEntities())
            {
                var wyszukaj = from p in db.Urlop
                               from c in db.Pracownicy
                               where p.Pracownicy_ID_pracownika == c.ID_pracownika && c.Pracownicy_ID_pracownika == ID_pracownika
                               select p;

                foreach (var _dane in wyszukaj)
                {
                    var d = _dane.Pracownicy.Imie;
                    d = _dane.Typ_Urlopu.nazwa;
                }

                _dane_Urlop = wyszukaj.ToList();
                try
                {
                    Urlop_DataGrid.Dispatcher.Invoke(new Action(() =>
                    {
                        Urlop_DataGrid.ItemsSource = _dane_Urlop;
                    }));
                }
                catch (Exception)
                {
                    //
                }


            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Win_profil_kierownik.ID_pracownika = ID_pracownika;
            var win = new Win_profil_kierownik(); //Administrator
            win.Show();
        }

        private void zadanieDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            czatDataGrid.ItemsSource = Filtrowanie_Czatu(_dane_czatu);
            if (zadanieDataGrid.SelectedIndex != -1)
            {
                czatDataGrid.Visibility = Visibility.Visible;
                Wiadomosc.Visibility = Visibility.Visible;
                Wyslij.Visibility = Visibility.Visible;
            }
            else
            {
                czatDataGrid.Visibility = Visibility.Hidden;
                Wiadomosc.Visibility = Visibility.Hidden;
                Wyslij.Visibility = Visibility.Hidden;
            }

        }

        private void Main_Closing(object sender, CancelEventArgs e)
        {
            flaga = false;
            Thread.Sleep(100);
        }

        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Wyslij_Click(null, null);
            }
        }
        //metoda do wysylania wiadomosci
        private void Wyslij_Click(object sender, RoutedEventArgs e)
        {
            flaga2 = true;

        }
        //anulowanie urlopu
        private void anuluj_Click(object sender, RoutedEventArgs e)
        {
            if (Urlop_DataGrid.SelectedItem != null)
            {
                using (var db = new BazaEntities())
                {
                    var edytuj = (Urlop)Urlop_DataGrid.SelectedItem;

                    var nowy = new Urlop
                    {
                        Data_rozpoczecia = edytuj.Data_rozpoczecia,
                        Data_zakonczenia = edytuj.Data_zakonczenia,
                        ID_Urlopu = edytuj.ID_Urlopu,
                        Notka = edytuj.Notka,
                        Pracownicy_ID_pracownika = edytuj.Pracownicy_ID_pracownika,
                        Typ_Urlopu_ID_typu_urlopu = 0
                    };

                    db.Urlop.Attach(nowy);
                    db.Entry(nowy).State = EntityState.Modified;

                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("aaaa");
                    }

                }
                return;
            }
            MessageBox.Show("Wybierz urlop, który chcesz anulować!");
        }
        //pusto
        private void Urlop_DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {

            var win = new Win_dodaj_zadanie();
            win.Show();
        }
        //Ładuje wszystkie informacje o zadaniu do grida
        private void zadanieDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (zadanieDataGrid2.SelectedIndex >= 0)
            {
                var getRow = (Zadanie)zadanieDataGrid2.CurrentCell.Item;

                txtOpis.Text = getRow.Opis;
                txtTypZad.Text = getRow.Typ_Zadania.nazwa;
                txtDataDod.Text = getRow.Data_Dodania.ToString();
                txtDataDead.Text = getRow.Data_Deadline.ToString();
                txtStatus.Text = getRow.Status.Nazwa;
                txtIDZad.Text = getRow.ID_zadania.ToString();

                using (var db = new BazaEntities())
                {
                    var wyszukaj = from p in db.Lista_Zadan
                                   where p.Zadanie_ID_zadania == getRow.ID_zadania
                                   select p;

                    Osob_przydz.Text = "";

                    foreach (var dane in wyszukaj)
                    {
                        Osob_przydz.Text += dane.Pracownicy.Imie + " " + dane.Pracownicy.Nazwisko + " " + dane.Zegar + "\n";
                    }

                }

            }
        }
        //zaladowanie informacji o ocenach
        private void ocenyDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ocenyDataGrid.SelectedIndex >= 0)
            {
                Raport_Button.IsEnabled = true;
                using (var db = new BazaEntities())
                {

                    getRow = (Oceny)ocenyDataGrid.CurrentCell.Item;

                    getRow = (from p in db.Oceny where p.ID_Oceny == getRow.ID_Oceny select p).FirstOrDefault();

                    if (getRow.Ocena == null)
                    {
                        Ocena_notatka.IsHitTestVisible = true;
                        Ocena_liczba.IsHitTestVisible = true;
                        ComboTypOceny.IsHitTestVisible = true;
                        Wystaw_ocene.IsEnabled = true;
                    }
                    else
                    {
                        Ocena_notatka.IsHitTestVisible = false;
                        Ocena_liczba.IsHitTestVisible = false;
                        ComboTypOceny.IsHitTestVisible = false;
                        Wystaw_ocene.IsEnabled = false;
                    }


                    Pracownik_ocena.Text = getRow.Lista_Zadan.Pracownicy.Imie + " " + getRow.Lista_Zadan.Pracownicy.Nazwisko;
                    Zadanie_Ocena.Text = getRow.Lista_Zadan.Zadanie.Opis;
                    Ocena_notatka.Text = getRow.Notka;
                    ComboTypOceny.Text = getRow.Typ_Oceny.nazwa;
                    Ocena_liczba.Text = getRow.Ocena.ToString();

                }

            }
        }

        private void Wystaw_ocene_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var d = decimal.Parse(Ocena_liczba.Text);
                if (d > 10 || d < 0)
                {
                    MessageBox.Show("Podaj liczbe w zakresie od 0 do 10 !");
                    return;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Podaj liczbe w zakresie od 0 do 10 !");
                return;
            }

            if (ComboTypOceny.SelectedIndex != -1)
                using (var db = new BazaEntities()) //laczymy sie z baza
                {

                    var ID_oceny = (int)getRow.ID_Oceny;

                    Oceny dodanie = null;

                    dodanie = new Oceny()
                    {
                        ID_Oceny = ID_oceny,
                        Notka = Ocena_notatka.Text,
                        Lista_Zadan_ID_listy =
                            (from p in db.Oceny where p.ID_Oceny == ID_oceny select p.Lista_Zadan_ID_listy)
                                .FirstOrDefault(),
                        Ocena = decimal.Parse(Ocena_liczba.Text),
                        Pracownicy_ID_pracownika = ID_pracownika,
                        Typ_Oceny_ID_typu_oceny = (ComboTypOceny.SelectedIndex + 1),

                    };

                    db.Oceny.Attach(dodanie);
                    db.Entry(dodanie).State = EntityState.Modified;

                    try
                    {
                        db.SaveChanges();
                        MessageBox.Show("Ocena została wystawiona");
                    }
                    catch (Exception)
                    {
                        db.Dispose();
                        MessageBox.Show("Brak połączenia z Baza!");
                    }
                }
            else MessageBox.Show("Proszę wybrać rodzaj oceny!");
        }
        //wysylanie raportu do admina
        private void Raport_Button_Click(object sender, RoutedEventArgs e)
        {
            var win = new Win_raport_do_admina(); //Administrator
            win.Show();
        }
        //nie tu
        private void Raport_Button_Click2(object sender, RoutedEventArgs e)
        {
            var win = new Win_raport_kierownik(); //Administrator
            win.Show();
        }

        private void Notatka_B_Click(object sender, RoutedEventArgs e)
        {
            if (txtIDZad.Text == "") return;
            Win_Notatka.ID_pracownika = ID_pracownika;
            Win_Notatka.ID_zadania = int.Parse(txtIDZad.Text);
            var win = new Win_Notatka(); //Administrator
            win.Show();
        }
        //edycja zadania
        private void button2_Click(object sender, RoutedEventArgs e)
        {
            if (txtIDZad.Text == "") return;
            Win_edytuj_zadanie.IDZadania = int.Parse(txtIDZad.Text);
            var Win = new Win_edytuj_zadanie();
            Win.Show();
        }

        private const double RequiredWidth = 1264;

        private const double RequiredHeight = 669;

        private void Main_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var c = new double[2];
            c[0] = ActualWidth / RequiredWidth;
            c[1] = ActualHeight / RequiredHeight;
            var d = c.Min();

            skala.ScaleX = d;
            skala.ScaleY = d;
        }

        private void Main_StateChanged(object sender, EventArgs e)
        {
            var c = new double[2];
            c[0] = ActualWidth / RequiredWidth;
            c[1] = ActualHeight / RequiredHeight;
            var d = c.Min();

            skala.ScaleX = d;
            skala.ScaleY = d;
        }

        private void logout_Click(object sender, RoutedEventArgs e)
        {
            var Win = new Win_logowanie();
            Win.Show();
            this.Close();
        }
    }
}
/*
         using (var db = new BazaEntities())//laczymy sie z baza
         {
             //Dodanie wiersza na przykladzie encji STANOWISKO
             var dodanie = new Stanowisko { ID_Stanowiska = 3, Nazwa = "AAA" }; // definiujemy wiersz


             db.Stanowisko.Add(dodanie); //Dodajemy wiersz do tabeli

             try // try aby nie zdarzyla sie sytuacja ze dodajemy wiersze ktore juz istnieja
             {
                 db.SaveChanges(); //Zapisuje zmiany w bazie dopiero teraz dane zostana zapisae w bazie
             }
             catch (Exception)
             {
                 db.Dispose();//Odrzucamy zmiany i rozlaczamy sie z baza
                 MessageBox.Show("Dane już znajduja sie w bazie");
             }

         }

         using (var db = new BazaEntities())//laczymy sie z baza
         {
             //Dodanie wiersza na przykladzie encji STANOWISKO
             var dodanie = new Stanowisko { ID_Stanowiska = 4, Nazwa = "Administrator" }; // definiujemy wiersz
             db.Stanowisko.Add(dodanie); //Dodajemy wiersz do tabeli

             try // dodanie damy w try aby nie zdarzyla sie sytuacja ze dajemy wiersze ktore juz istnieja
             {
                 db.SaveChanges(); //Zapisuje zmiany w bazie dopiero teraz dane zostana zapisae w bazie
             }
             catch (Exception)
             {
                 db.Dispose();//Odrzucamy zmiany i rozlaczamy sie z baza
                 MessageBox.Show("Dane już znajduja sie w bazie");
             }

         }


         using (var db = new BazaEntities())//laczymy sie z baza
         {
             //Wyciaganie informacji z bazy
             var wyszukaj = db.Stanowisko.Where(c => c.ID_Stanowiska == 3); // szukamy w encji stanowiska o nr3
             var dane = wyszukaj.FirstOrDefault<Stanowisko>(); //z wynikow jakie mamy wybieramy 1 wartosc
             Pole.Text = dane.ID_Stanowiska.ToString() + " " + dane.Nazwa.ToString();

             //Zmiana wartosci wiersza (Update)
             var update = new Stanowisko { ID_Stanowiska = 1, Nazwa = "CCC" };//Zamiast RRR mozna wpisac inna wartosc
             db.Stanowisko.Attach(update);
             db.SaveChanges();

             //Ponownie wyswietlimy zmiany
             wyszukaj = db.Stanowisko.Where(c => c.ID_Stanowiska == 1); // szukamy w encji stanowiska o nr1
             dane = wyszukaj.FirstOrDefault<Stanowisko>(); //z wynikow jakie mamy wybieramy 1 wartosc
             Pole.Text += "\n" + dane.ID_Stanowiska.ToString() + " " + dane.Nazwa.ToString();


             //Usuwanie Najpierw szukamy goscia do usuwania a potem go usuwamy
             wyszukaj = db.Stanowisko.Where(c => c.ID_Stanowiska == 4); // szukamy w encji stanowiska o nr4
             dane = wyszukaj.FirstOrDefault<Stanowisko>(); //z wynikow jakie mamy wybieramy 1 wartosc
             db.Stanowisko.Remove(dane);
             db.SaveChanges();

             //Ponownie wyswietlimy zmiany
             wyszukaj = db.Stanowisko.Where(c => c.ID_Stanowiska == 4); // szukamy w encji stanowiska o nr4
             dane = wyszukaj.FirstOrDefault<Stanowisko>(); //z wynikow jakie mamy wybieramy 1 wartosc
             if (dane == null) Pole.Text += "\nBrak danych"; // _dane zwracaja wartosc null jesli nie ma tej osoby

         }*/
