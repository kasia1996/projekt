﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace System_zarzadania_zadaniami.Okna
{
    /// <summary>
    /// Interaction logic for Win_profil_kierownik.xaml
    /// </summary>
    public partial class Win_profil_pracownik : Window
    {

        public static int ID_pracownika = 1;
        private List<Urlop> _dane_Urlop;


        public Win_profil_pracownik()
        {
            InitializeComponent();
        }

        private void Tab_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var _nazwa = Tab.SelectedItem;
            Text_tab.Text = ((TabItem)_nazwa).Header.ToString();
        }

        private void ladowaie_danych_urlop()
        {
            using (var db = new BazaEntities())
            {
                var wyszukaj = from p in db.Urlop
                               from c in db.Typ_Urlopu
                               orderby p.Data_rozpoczecia
                               where p.Typ_Urlopu_ID_typu_urlopu == c.ID_typu_urlopu && p.Pracownicy_ID_pracownika == ID_pracownika
                               select p;

                foreach (var _dane in wyszukaj)
                {
                    var d = _dane.Typ_Urlopu.nazwa;
                }

                var wyszukaj2 = from p in db.Typ_Urlopu select p;

                foreach (var _dane in wyszukaj2)
                {
                    ComboTypUrlopu.Items.Add(_dane.nazwa);
                }


                _dane_Urlop = wyszukaj.ToList();

                Urlop_DataGrid.ItemsSource = _dane_Urlop;

            }
        }

        private void ladowanie_ocen()
        {
            using (var db = new BazaEntities())
            {
                var wyszukaj = from x in db.Oceny
                               where x.Lista_Zadan.Pracownicy_ID_pracownika == ID_pracownika
                               select x;

                foreach (var _dane in wyszukaj)
                {
                    var d = _dane.Lista_Zadan.Zadanie.Opis;
                    d = _dane.Pracownicy.Imie;
                    d = _dane.Pracownicy.Nazwisko;
                    d = _dane.Typ_Oceny.nazwa;

                }
                ocenyDataGrid.ItemsSource = wyszukaj.ToList();

            }
        }
        private void LoadDanePracownika()
        {
            var IdPracownika = Win_logowanie.IdPracownika;
            //Test
            //IdPracownika = 2;
            using (var db = new BazaEntities())
            {
                var wyciag1 = from p in db.Stanowisko select p.Nazwa;
                var wyciag2 = from p in db.Pracownicy select p.Nazwisko;
                var wyciag3 = from r in db.Pracownicy select r.Imie;
                var kierownik = wyciag2.ToList().Zip(wyciag3.ToList(), (n, i) => n + " " + i);

                var wyszukaj = from p in db.Pracownicy where p.ID_pracownika == IdPracownika select p;
                var dane = wyszukaj.ToList();
                ImieLabel.Content = dane[0].Imie;
                NazwiskoLabel.Content = dane[0].Nazwisko;
                HasloLabel.Content = dane[0].haslo;
                DataUrodzeniaLabel.Content = dane[0].Data_urodzenia.ToShortDateString();
                DataZatrudnieniaLabel.Content = dane[0].Data_zatrudnienia.ToShortDateString();
                StanowiskoLabel.Content = dane[0].Stanowisko.Nazwa;
                if (dane[0].Pracownicy2 != null)
                    KierownikLabel.Content = dane[0].Pracownicy2.Nazwisko + " " + dane[0].Pracownicy2.Imie;
                else
                    KierownikLabel.Content = "";


            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            System.Windows.Data.CollectionViewSource urlopViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("urlopViewSource")));


            ladowaie_danych_urlop();
            System.Windows.Data.CollectionViewSource bazaEntitiesViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("bazaEntitiesViewSource")));
            ladowanie_ocen();
            LoadDanePracownika();
            // Load data by setting the CollectionViewSource.Source property:
            // bazaEntitiesViewSource.Source = [generic data source]
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            if (Data_rozp.Text != "" && Data_zak.Text != "" && ComboTypUrlopu.SelectedIndex != -1)
            {
                using (var db = new BazaEntities())
                {
                    var id = 0;
                    var wyszukaj = (from p in db.Urlop select p.ID_Urlopu);

                    try
                    {
                        if (wyszukaj != null) id = (int)wyszukaj.Max();
                    }
                    catch (Exception)
                    {

                        //
                    }



                    id++;

                    var dodaj = new Urlop
                    {
                        Data_rozpoczecia = Data_rozp.SelectedDate.Value,
                        Data_zakonczenia = Data_zak.SelectedDate,
                        ID_Urlopu = id,
                        Notka = UrlopNotatka.Text,
                        Pracownicy_ID_pracownika = ID_pracownika,
                        Typ_Urlopu_ID_typu_urlopu = ComboTypUrlopu.SelectedIndex

                    };

                    db.Urlop.Add(dodaj);
                    try
                    {
                        db.SaveChanges();
                        MessageBox.Show("Wysłano zapytanie");
                        UrlopNotatka.Text = "";
                        Data_rozp.Text = "";
                        Data_zak.Text = "";
                        ComboTypUrlopu.SelectedIndex = -1;
                    }
                    catch (Exception)
                    {

                        db.Dispose();
                        MessageBox.Show("Brak polaczenia z Baza");
                    }

                }
            }
        }

        private const double RequiredWidth = 1264;

        private const double RequiredHeight = 669;

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var c = new double[2];
            c[0] = ActualWidth / RequiredWidth;
            c[1] = ActualHeight / RequiredHeight;
            var d = c.Min();

            skala.ScaleX = d;
            skala.ScaleY = d;
        }

        private void Window_StateChanged(object sender, EventArgs e)
        {
            var c = new double[2];
            c[0] = ActualWidth / RequiredWidth;
            c[1] = ActualHeight / RequiredHeight;
            var d = c.Min();

            skala.ScaleX = d;
            skala.ScaleY = d;
        }
    }
}
