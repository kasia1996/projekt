﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace System_zarzadania_zadaniami.Okna
{
    /// <summary>
    /// Interaction logic for Win_dodaj_zadanie.xaml
    /// </summary>
    public partial class Win_dodaj_zadanie : Window
    {

        public Win_dodaj_zadanie()
        { 

            InitializeComponent();
            using (var db = new BazaEntities())
            {
                var wyszukaj = from p in db.Pracownicy where p.Pracownicy_ID_pracownika == Win_logowanie.IdPracownika select p;

                foreach (var dane in wyszukaj)
                {
                    var d = dane.Stanowisko.Nazwa;

                }

                Pracownicy_DataGrid.ItemsSource = wyszukaj.ToList();
                var wyszukaj2 = from p in db.Typ_Zadania select p;
                foreach (var dane in wyszukaj2)
                {
                    typZadania.Items.Add(dane.nazwa);
                }

                for (var i = 1; i < 5; i++) priorytetZadania.Items.Add(i);
            }

        }



        private void button_Click(object sender, RoutedEventArgs e)
        {
            if (opisZadania.Text == "" || DateTime.Parse(dataPlanowana.Text) < DateTime.Today || priorytetZadania.SelectedIndex == -1 || typZadania.SelectedIndex == -1)
            {
                MessageBox.Show("Wybierz wszystkie dane");
                return;
            }
            using (var db = new BazaEntities())                     //laczymy sie z baza
            {
                var wyszukaj = from z in db.Zadanie select z.ID_zadania;
                decimal dane = 0;
                try
                {
                    dane = wyszukaj.Max();
                }
                catch (Exception)
                {

                    //
                }
                //rekord z najwyzszym ID
                var ID_zadania_temp = dane + 1;          //przypisuje kolejny numer ID
                var idTypZadania = from p in db.Typ_Zadania where p.nazwa == typZadania.Text select p.ID_typu_zadania;
                var id = idTypZadania.FirstOrDefault();

                var dodaj = new Zadanie
                {
                    ID_zadania = ID_zadania_temp,
                    Opis = opisZadania.Text,
                    Priorytet = int.Parse(priorytetZadania.Text),
                    Data_Dodania = System.DateTime.Now,
                    Data_Deadline = dataPlanowana.SelectedDate.Value,
                    Pracownicy_ID_pracownika = Win_logowanie.IdPracownika,

                    Status_ID_statusu = 1,
                    Typ_Zadania_ID_typu_zadania = id,


                };
                db.Zadanie.Add(dodaj);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception)
                {
                    MessageBox.Show("Problem połączenia z baz2");
                }
                var wyszukaj2 = from z in db.Lista_Zadan select z.ID_listy;
                decimal dane3 = 0;
                try
                {
                    dane3 = wyszukaj2.Max();
                }
                catch (Exception)
                {
                    //
                }//rekord z najwyzszym ID
                var IdListy = dane3 + 1;          //przypisuje kolejny numer ID 
                foreach (var dane2 in Pracownicy_DataGrid.SelectedItems)
                {
                    var pracownik = dane2 as Pracownicy;

                    var dodaj2 = new Lista_Zadan
                    {
                        Zegar = TimeSpan.Parse("0"),
                        ID_listy = IdListy,
                        Zadanie_ID_zadania = ID_zadania_temp,
                        Pracownicy_ID_pracownika = pracownik.ID_pracownika,

                    };
                    db.Lista_Zadan.Add(dodaj2);
                    IdListy++;
                }
                try
                {
                    db.SaveChanges();
                    this.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("Problem połączenia z bazą");
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            System.Windows.Data.CollectionViewSource pracownicyViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("pracownicyViewSource")));


            // Load data by setting the CollectionViewSource.Source property:
            // pracownicyViewSource.Source = [generic data source]
        }

        private void Pracownicy_DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void typZadania_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var combo = (sender as ComboBox);

            if (combo.SelectedIndex == 0) Pracownicy_DataGrid.SelectionMode = DataGridSelectionMode.Single;
            else Pracownicy_DataGrid.SelectionMode = DataGridSelectionMode.Extended;
        }
    }
}
