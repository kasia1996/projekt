﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace System_zarzadania_zadaniami.Okna
{
    /// <summary>
    /// Interaction logic for Win_dodaj_zadanie.xaml
    /// </summary>
    public partial class Win_edytuj_zadanie : Window
    {
        public static int IDZadania;
        Zadanie dane2;
        List<Lista_Zadan> pracownicyZadania;

        public Win_edytuj_zadanie()
        {

            InitializeComponent();
            using (var db = new BazaEntities())
            {
                var wyszukaj = from p in db.Pracownicy where p.Pracownicy_ID_pracownika == Win_logowanie.IdPracownika select p;

                foreach (var dane in wyszukaj)
                {
                    var d = dane.Stanowisko.Nazwa;

                }

                Pracownicy_DataGrid.ItemsSource = wyszukaj.ToList();
                var wyszukaj2 = from p in db.Typ_Zadania select p;
                foreach (var dane in wyszukaj2)
                {
                    typZadania.Items.Add(dane.nazwa);
                }

                var wyszukaj5 = from p in db.Status select p;
                foreach (var dane in wyszukaj5)
                {
                    Status_zadania.Items.Add(dane.Nazwa);
                }

                for (var i = 1; i < 5; i++) priorytetZadania.Items.Add(i);

                var wyszukaj3 = from p in db.Zadanie where p.ID_zadania == IDZadania select p;
                dane2 = wyszukaj3.FirstOrDefault();

                Status_zadania.SelectedIndex = (int)(dane2.Status_ID_statusu - 1);
                opisZadania.Text = dane2.Opis;
                priorytetZadania.Text = dane2.Priorytet.ToString();
                typZadania.SelectedIndex = (int)(dane2.Typ_Zadania_ID_typu_zadania - 1);
                dataPlanowana.Text = dane2.Data_Deadline.ToString();
                var wyszukaj4 = from p in db.Lista_Zadan where p.Zadanie_ID_zadania == IDZadania select p;
                foreach (var dane3 in wyszukaj4)
                {
                    akt_prac.Text += dane3.Pracownicy.Imie + ' ' + dane3.Pracownicy.Nazwisko + "\n";
                }
                pracownicyZadania = wyszukaj4.ToList();

            }

        }


        //edycja zadania
        private void button_Click(object sender, RoutedEventArgs e)
        {
            using (var db = new BazaEntities())                     //laczymy sie z baza
            {
                //przypisuje kolejny numer ID
                var idTypZadania = from p in db.Typ_Zadania where p.nazwa == typZadania.Text select p.ID_typu_zadania;
                var id = idTypZadania.FirstOrDefault();
                var idstatusu = from p in db.Status where p.Nazwa == Status_zadania.Text select p.ID_statusu;
                var id2 = idstatusu.FirstOrDefault();
                var dodaj = new Zadanie();

                if (id2 > 2)
                    dodaj = new Zadanie
                    {
                        ID_zadania = IDZadania,
                        Opis = opisZadania.Text,
                        Priorytet = int.Parse(priorytetZadania.Text),
                        Data_Dodania = dane2.Data_Dodania,
                        Data_Deadline = dataPlanowana.SelectedDate.Value,
                        Pracownicy_ID_pracownika = Win_logowanie.IdPracownika,

                        Status_ID_statusu = id2,
                        Typ_Zadania_ID_typu_zadania = id,


                    };
                else
                    dodaj = new Zadanie
                    {
                        ID_zadania = IDZadania,
                        Opis = opisZadania.Text,
                        Priorytet = int.Parse(priorytetZadania.Text),
                        Data_Dodania = dane2.Data_Dodania,
                        Data_Deadline = dataPlanowana.SelectedDate.Value,
                        Pracownicy_ID_pracownika = Win_logowanie.IdPracownika,
                        Data_zakonczenia = DateTime.Today,
                        Status_ID_statusu = id2,
                        Typ_Zadania_ID_typu_zadania = id,


                    };
                db.Zadanie.Attach(dodaj);
                db.Entry(dodaj).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception)
                {
                    // MessageBox.Show("Brak połączenia z bazą!");
                }
                var id_p = 0;
                try
                {
                    var wyszukaj_id = (from p in db.Powiadomienia select p.ID_powiadomienia).Max();
                    id_p = (int)wyszukaj_id;

                }
                catch (Exception)
                {

                    //
                }

                var wyszukaj2 = from q in db.Lista_Zadan where q.Zadanie_ID_zadania == IDZadania orderby q.ID_listy select q;
                for (int i = 0; i < wyszukaj2.Count(); i++)
                {
                    var daneq = wyszukaj2.Skip(i).First();
                    Powiadomienia update = new Powiadomienia();
                    id_p++;
                    update.ID_powiadomienia = id_p;
                    update.Opis = "Zadanie o ID: " + IDZadania.ToString() + " zostało edytowane.";
                    update.Lista_Zadan_ID_listy = daneq.ID_listy;
                    db.Powiadomienia.Add(update);
                    db.Entry(update).State = System.Data.EntityState.Added;
                }
                try
                {
                    db.SaveChanges();

                }
                catch (Exception)
                {
                    //   MessageBox.Show("Problem z połączeniem z bazą!");
                }
                if (Pracownicy_DataGrid.SelectedIndex == -1)
                {
                    this.Close();
                    return;
                }
                var flaga = false;

                foreach (var dane22 in pracownicyZadania)
                {
                    flaga = false;
                    foreach (var dane3 in Pracownicy_DataGrid.SelectedItems)
                    {
                        var dane4 = dane3 as Pracownicy;

                        if (dane22.Pracownicy.ID_pracownika == dane4.ID_pracownika)
                        {
                            flaga = true;
                        }

                    }
                    if (!flaga)
                    {
                        var wyszukaj9 = from p in db.Lista_Zadan where p.ID_listy == dane22.ID_listy select p;
                        var dane9 = wyszukaj9.FirstOrDefault();

                        var wyszukaj7 = from p in db.Powiadomienia
                                        where p.Lista_Zadan_ID_listy == dane22.ID_listy
                                        select p;
                        foreach (var danee in wyszukaj7)
                        {
                            try
                            {
                                db.Powiadomienia.Remove(danee);
                                db.Entry(danee).State = EntityState.Deleted;
                                db.SaveChanges();
                            }
                            catch (Exception)
                            {
                                //  MessageBox.Show("Brak połączenia z bazą!");
                            }
                        }
                        try
                        {

                            db.Lista_Zadan.Remove(dane9);
                            db.Entry(dane9).State = EntityState.Deleted;
                            db.SaveChanges();
                        }
                        catch (Exception)
                        {
                            //   MessageBox.Show("Brak połączenia z bazą!");
                        }
                    }
                }




                foreach (var dane3 in Pracownicy_DataGrid.SelectedItems)
                {
                    var dane4 = dane3 as Pracownicy;
                    flaga = false;
                    foreach (var dane23 in pracownicyZadania)
                    {


                        if (dane23.Pracownicy.ID_pracownika == dane4.ID_pracownika)
                        {
                            flaga = true;
                        }

                    }
                    if (!flaga)
                    {
                        decimal id3 = 0;
                        try
                        {
                            id3 = (from p in db.Lista_Zadan select p.ID_listy).Max();
                        }
                        catch (Exception)
                        {
                            //
                        }

                        id3 += 1;
                        var dodaj5 = new Lista_Zadan
                        {
                            ID_listy = id3,
                            Zegar = TimeSpan.Parse("0"),
                            Zadanie_ID_zadania = IDZadania,
                            Pracownicy_ID_pracownika = dane4.ID_pracownika,
                        };

                        db.Lista_Zadan.Add(dodaj5);
                        try
                        {
                            db.SaveChanges();

                        }
                        catch (Exception)
                        {
                            //  MessageBox.Show("Błąd!");
                        }
                    }
                }

            }
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            System.Windows.Data.CollectionViewSource pracownicyViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("pracownicyViewSource")));


            // Load data by setting the CollectionViewSource.Source property:
            // pracownicyViewSource.Source = [generic data source]
        }

        private void Pracownicy_DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        // jesli typ zadania jest drugi to mozna wybierac wielu pracownikow do zadania
        private void typZadania_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var combo = (sender as ComboBox);

            if (combo.SelectedIndex == 0) Pracownicy_DataGrid.SelectionMode = DataGridSelectionMode.Single;
            else Pracownicy_DataGrid.SelectionMode = DataGridSelectionMode.Extended;
        }
    }
}
