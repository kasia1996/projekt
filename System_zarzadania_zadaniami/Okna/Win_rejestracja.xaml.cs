﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace System_zarzadania_zadaniami.Okna
{
    /// <summary>
    /// Interaction logic for Win_rejestracja.xaml
    /// </summary>
    public partial class Win_rejestracja : Window
    {
        public Win_rejestracja()
        {
            InitializeComponent();
        }
        private void WinLoaded(object sender, RoutedEventArgs e)
        {
            InitializeComponent();
            using (var db = new BazaEntities())
            {
                //var wyciag1 = from p in db.Stanowisko select p.Nazwa;
                // StanowiskoComboBox.ItemsSource = wyciag1.ToList();
                // var wyciag2 = from p in db.Pracownicy where (p.Stanowisko_ID_Stanowiska==2 || p.Stanowisko_ID_Stanowiska == 3) select p.Nazwisko;
                // var wyciag3 = from r in db.Pracownicy where (r.Stanowisko_ID_Stanowiska == 2 || r.Stanowisko_ID_Stanowiska == 3) select r.Imie;
                // var kierownik = wyciag2.ToList().Zip(wyciag3.ToList(), (n, i) => n + " " + i);
                // KierownikComboBox.Items.Add("");
                // foreach (var kier in kierownik)
                // {
                //     KierownikComboBox.Items.Add(kier);
                // }
                var wyszukaj = from p in db.Pracownicy where p.Stanowisko_ID_Stanowiska > 1 select p;
                KierownikComboBox.Items.Add("");
                foreach (var kier in wyszukaj)
                {
                    KierownikComboBox.Items.Add(kier.Imie + " " + kier.Nazwisko);
                }

            }
        }
        private void DodajPracownikaButton_Click(object sender, RoutedEventArgs e)
        {

            if (ImieTextBox.Text == "" ||
                NazwiskoTextBox.Text == "" ||
                HasloTextBox.Password == "" ||
                DataUrodzeniaDatePicker.Text == "" ||
                DataZatrudnieniaDatePicker.Text == ""
                )
                MessageBox.Show("Prosze wypełnić wszystkie potrzebne dane");
            else
            {
                using (var db = new BazaEntities())
                {
                    var wyciag1 = from p in db.Pracownicy select p.ID_pracownika;
                    var listaID = wyciag1.ToList();
                    //var wyciag2 = from p in db.Stanowisko where p.Nazwa == StanowiskoComboBox.Text select p.ID_Stanowiska;
                    //var stanowiskoID = wyciag1.ToList();
                    var kierownikNazwisko = KierownikComboBox.Text.Split(' ').First();
                    var kierownikImie = KierownikComboBox.Text.Split(' ').Last();
                    var wyciag3 = from p in db.Pracownicy where p.Nazwisko == kierownikNazwisko && p.Imie == kierownikImie select p.ID_pracownika;
                    var kierownikID = wyciag3.ToList();
                    //var wyciag3 = from p in db.Pracownicy where p.Nazwisko == NazwiskoTextBox.Text
                    /*
                        public decimal ID_pracownika { get; set; }
                        public string haslo { get; set; }
                        public string Imie { get; set; }
                        public string Nazwisko { get; set; }
                        public System.DateTime Data_urodzenia { get; set; }
                        public System.DateTime Data_zatrudnienia { get; set; }
                        public Nullable<decimal> Pracownicy_ID_pracownika { get; set; }
                        public int Stanowisko_ID_Stanowiska { get; set; }
                        */
                    Pracownicy nowyPracownik;
                    if (kierownikID.FirstOrDefault() != 0)
                    {
                        nowyPracownik = new Pracownicy
                        {
                            ID_pracownika = listaID.Last() + 1,
                            haslo = HasloTextBox.Password,
                            Imie = ImieTextBox.Text,
                            Nazwisko = NazwiskoTextBox.Text,
                            Data_urodzenia = DataUrodzeniaDatePicker.SelectedDate.Value,
                            Data_zatrudnienia = DataZatrudnieniaDatePicker.SelectedDate.Value,
                            Pracownicy_ID_pracownika = Decimal.ToInt32(kierownikID.FirstOrDefault()),
                            Stanowisko_ID_Stanowiska = 0

                        };
                    }
                    else
                    {
                        nowyPracownik = new Pracownicy
                        {
                            ID_pracownika = listaID.Last() + 1,
                            haslo = HasloTextBox.Password,
                            Imie = ImieTextBox.Text,
                            Nazwisko = NazwiskoTextBox.Text,
                            Data_urodzenia = DataUrodzeniaDatePicker.SelectedDate.Value,
                            Data_zatrudnienia = DataZatrudnieniaDatePicker.SelectedDate.Value,
                            Stanowisko_ID_Stanowiska = 0

                        };
                    }
                    db.Pracownicy.Add(nowyPracownik);
                    try
                    {
                        db.SaveChanges();
                        this.Close();
                    }
                    catch (Exception)
                    {
                        db.Dispose();
                        MessageBox.Show("Taki pracownik już istnieje");
                    }
                }
            }

        }

    }


}
