﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Objects;
using System.Data.EntityClient;
using System.Data;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System_zarzadania_zadaniami.Okna;

namespace System_zarzadania_zadaniami
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //===========================================================
        Thread _watek_czatu;
        private bool flaga = true;
        private bool flaga2;
        Stopwatch _zegar_watku_czatu = new Stopwatch();
        private List<Czat> _dane_czatu;
        //===============================================  Czat MERYX
        Stopwatch timerZadania = new Stopwatch();
        TimeSpan timeOfZadanie = new TimeSpan(0, 0, 0);
        Lista_Zadan update = new Lista_Zadan();
        Powiadomienia dane = new Powiadomienia();
        private bool czy_Wyswietlone = false;




        private int ID_pracownika = Win_logowanie.IdPracownika;
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        public MainWindow()
        {
            InitializeComponent();
            Wiadomosc.MaxLength = 254;
            _watek_czatu = new Thread(Procedura_watku_czatu);
            _watek_czatu.Start();
            czatDataGrid.Visibility = Visibility.Hidden;
            Wiadomosc.Visibility = Visibility.Hidden;
            Wyslij.Visibility = Visibility.Hidden;
        }

           // odswiezenie czatu
        private void Odswiezenie_danych_Czatu()
        {
            using (var db = new BazaEntities()) //laczymy sie z baza
            {
                var wyszukaj = from p in db.Czat orderby p.Data_wyslania select p;

                foreach (var dane in wyszukaj)
                {
                    var d = dane.Pracownicy.Imie;
                    d = dane.Pracownicy.Nazwisko;
                }
                var dane2 = wyszukaj.ToList();
                _dane_czatu = dane2;


                czatDataGrid.ItemsSource = Filtrowanie_Czatu(dane2);
            }
        }
        //Watek, ktory co pewien czas odswieza datagrid i dodaje nowe wiadomosci
        private void Procedura_watku_czatu()
        {
            while (flaga)
            {
                txtTimer.Dispatcher.Invoke(new Action(() =>
                {
                    txtTimer.Text = timeOfZadanie.Add(timerZadania.Elapsed).ToString();
                }));
                if (flaga2)
                {
                    using (var db = new BazaEntities()) //laczymy sie z baza
                    {
                        IList wybrany = null;
                        zadanieDataGrid_wiad.Dispatcher.Invoke(new Action(() =>
                        {

                            wybrany = zadanieDataGrid_wiad.SelectedItems;
                        }));
                        var zadanie = (Zadanie)wybrany[0];
                        var ID_zadania = (int)zadanie.ID_zadania;

                        decimal wyszukaj = 0;
                        try
                        {
                            wyszukaj = (from p in db.Czat select p.ID_wiadomosci).Max();
                        }
                        catch (Exception)
                        {

                        }
                        Czat dodanie = null;
                        try
                        {
                            Wiadomosc.Dispatcher.Invoke(new Action(() =>
                            {
                                dodanie = new Czat()
                                {
                                    ID_wiadomosci = ((int)wyszukaj) + 1,
                                    Data_wyslania = DateTime.Now,
                                    Tresc = Wiadomosc.Text,
                                    Zadanie_ID_zadania = ID_zadania,
                                    Pracownicy_ID_pracownika = ID_pracownika,

                                };
                                Wiadomosc.Text = "";
                            }));
                        }
                        catch (Exception)
                        {

                            //
                        }

                        db.Czat.Add(dodanie);

                        try
                        {
                            db.SaveChanges();
                        }
                        catch (Exception)
                        {
                            db.Dispose();

                        }
                        try
                        {
                            czatDataGrid.Dispatcher.Invoke(new Action(() =>
                            {
                                Odswiezenie_danych_Czatu();
                            }));
                        }
                        catch (Exception)
                        {

                            //
                        }

                        flaga2 = false;
                    }
                }


                if (_zegar_watku_czatu.Elapsed.Seconds == 10)
                {
                    _zegar_watku_czatu.Restart();
                    czatDataGrid.Dispatcher.Invoke(new Action(() =>
                    {
                        using (var db = new BazaEntities()) //laczymy sie z baza
                        {
                            var wyszukaj = from p in db.Czat orderby p.Data_wyslania select p;

                            foreach (var dane in wyszukaj)
                            {
                                var d = dane.Pracownicy.Imie;
                                d = dane.Pracownicy.Nazwisko;
                            }
                            var dane2 = wyszukaj.ToList();
                            _dane_czatu = dane2;

                            czatDataGrid.ItemsSource = Filtrowanie_Czatu(dane2);
                        }
                    }));
                    ladowanie_zadan();
                    if (czy_Wyswietlone)
                    {
                        this.Dispatcher.Invoke(new Action(() =>
                            Powiadomienia()
                            ));
                    }
                }

            }
        }
        //metoda do wypisywania nazwy zakładki w ktorej sie znajdujemy
        private void Tab_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var _nazwa = Tab.SelectedItem;
            Text_tab.Text = ((TabItem)_nazwa).Header.ToString();
        }

        public ICollectionView Filtrowanie_Czatu(List<Czat> Czat)
        {
            if (zadanieDataGrid_wiad.SelectedIndex == -1) return null;
            IList wybrany = zadanieDataGrid_wiad.SelectedItems;
            var zadanie = (Zadanie)wybrany[0];
            var ID_zadania = (int)zadanie.ID_zadania;
            var filtr = new Predicate<object>(i => ((Czat)i).Zadanie_ID_zadania == ID_zadania);

            var dane2 = Czat;
            var itemSourceList = new CollectionViewSource() { Source = dane2 };
            var itemlist = itemSourceList.View;
            itemlist.Filter = filtr;

            return itemlist;
        }

        private void Main_Loaded(object sender, RoutedEventArgs e)
        {

            System.Windows.Data.CollectionViewSource zadanieViewSource =
                ((System.Windows.Data.CollectionViewSource)(this.FindResource("zadanieViewSource")));



            // statusViewSource.Source = [generic data source]

            ladowanie_zadan();
            LadowanieDanychWiadomosci();
            Powiadomienia();
            LadowanieRaporty();
        }

        private void ladowanie_zadan()
        {
            using (var db = new BazaEntities())
            {

                var wyszukaj = from p in db.Zadanie
                               join z in db.Lista_Zadan on p.ID_zadania equals z.Zadanie_ID_zadania
                               where z.Pracownicy_ID_pracownika == ID_pracownika && p.Status_ID_statusu != 2
                               orderby p.Priorytet
                               select p;

                foreach (var _dane in wyszukaj)
                {
                    var d = _dane.Status.Nazwa;
                    d = _dane.Typ_Zadania.nazwa;
                    d = _dane.Pracownicy.Nazwisko;
                    d = _dane.Pracownicy.Imie;


                }
                var dane = wyszukaj.ToList();

                try
                {
                    zadanieDataGrid.Dispatcher.Invoke(new Action(() =>
                    {
                        zadanieDataGrid.ItemsSource = dane;
                        zadanieDataGrid.IsHitTestVisible = zadanieDataGrid.HasItems;
                    }));
                }
                catch (Exception)
                {
                    //
                }


            }
        }

        private void LadowanieDanychWiadomosci()
        {
            using (var db = new BazaEntities()) //laczymy sie z baza
            {
                var wyszukaj = from p in db.Zadanie
                               from c in db.Lista_Zadan
                               where
                                   p.ID_zadania == c.Zadanie_ID_zadania && c.Pracownicy_ID_pracownika == ID_pracownika &&
                                   p.Status_ID_statusu != 2
                               select p;

                foreach (var dane in wyszukaj)
                {
                    var d = dane.Pracownicy.Nazwisko;
                }

                zadanieDataGrid_wiad.ItemsSource = wyszukaj.ToList();
            }
            System.Windows.Data.CollectionViewSource czatViewSource =
                ((System.Windows.Data.CollectionViewSource)(this.FindResource("czatViewSource")));

            using (var db = new BazaEntities()) //laczymy sie z baza
            {
                var wyszukaj = from p in db.Czat orderby p.Data_wyslania select p;

                foreach (var dane in wyszukaj)
                {
                    var d = dane.Pracownicy.Imie;
                    d = dane.Pracownicy.Nazwisko;
                }
                var dane2 = wyszukaj.ToList();
                _dane_czatu = dane2;
            }
            _zegar_watku_czatu.Start();
        }

        private void LadowanieRaporty()
        {
            using (var db = new BazaEntities())
            {
                var wyszukaj = from p in db.Lista_Zadan
                               where p.Pracownicy_ID_pracownika == ID_pracownika && p.Zadanie.Status_ID_statusu == 2

                               select p;
                var wyszukaj2 = from p in db.Raport
                                where p.Pracownicy_ID_pracownika == ID_pracownika
                                select p.Zadanie_ID_zadania;
                var dane = wyszukaj2.ToList();

                foreach (var kier in wyszukaj)
                {
                    if (!dane.Exists(x => x.ToString() == kier.Zadanie_ID_zadania.ToString()))
                        RaportcomboBox.Items.Add(kier.Zadanie_ID_zadania);
                }
            }
        }

        //laduje i wyswietla powiadomienia
        private void Powiadomienia()
        {
            using (var db = new BazaEntities())
            {
                var wyszukaj = from p in db.Powiadomienia
                               join z in db.Lista_Zadan on p.Lista_Zadan_ID_listy equals z.ID_listy
                               join q in db.Zadanie on z.Zadanie_ID_zadania equals q.ID_zadania
                               where (ID_pracownika == z.Pracownicy_ID_pracownika)
                               select p;
                if (wyszukaj.Any())
                {
                    czy_Wyswietlone = false;
                    dane = wyszukaj.FirstOrDefault();
                    var win = new Win_powiadomienie(dane);
                    win.Show();
                    win.Focus();
                    win.Closed += Win_Closed;
                }
            }
        }

        private void Win_Closed(object sender, EventArgs e)
        {
            czy_Wyswietlone = true;
        }

        //Ładuje wszystkie informacje o zadaniu do grida
        private void zadanieDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (zadanieDataGrid.SelectedIndex >= 0)
            {
                var getRow = (Zadanie)zadanieDataGrid.CurrentCell.Item;
                txtNazw.Text = getRow.Pracownicy.Nazwisko;
                txtImie.Text = getRow.Pracownicy.Imie;
                txtOpis.Text = getRow.Opis;
                txtTypZad.Text = getRow.Typ_Zadania.nazwa;
                txtDataDod.Text = getRow.Data_Dodania.ToString();
                txtDataDead.Text = getRow.Data_Deadline.ToString();
                txtStatus.Text = getRow.Status.Nazwa;
                txtIDZad.Text = getRow.ID_zadania.ToString();
                txtIDPrac.Text = getRow.Pracownicy_ID_pracownika.ToString();
                txtPriorytet.Text = getRow.Priorytet.ToString();

                using (var db = new BazaEntities())
                {
                    var wyszukaj = from p in db.Lista_Zadan
                                   where (getRow.ID_zadania == p.Zadanie_ID_zadania && p.Pracownicy_ID_pracownika == ID_pracownika)
                                   select p;
                    update = wyszukaj.FirstOrDefault();
                    txtTimer.Text = update.Zegar.Value.ToString();
                    timeOfZadanie = update.Zegar.GetValueOrDefault();
                }
            }
        }
        //metoda do wysylania wiadomosci
        private void Wyslij_Click(object sender, RoutedEventArgs e)
        {
            flaga2 = true;
        }
        //metoda do wysylania wiadomosci przy pomocy entera
        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Wyslij_Click(null, null);
            }
        }
        //Ładuje wiadomosci z zadania
        private void zadanieDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            czatDataGrid.ItemsSource = Filtrowanie_Czatu(_dane_czatu);
            if (zadanieDataGrid_wiad.SelectedIndex != -1)
            {
                czatDataGrid.Visibility = Visibility.Visible;
                Wiadomosc.Visibility = Visibility.Visible;
                Wyslij.Visibility = Visibility.Visible;
            }
            else
            {
                czatDataGrid.Visibility = Visibility.Hidden;
                Wiadomosc.Visibility = Visibility.Hidden;
                Wyslij.Visibility = Visibility.Hidden;
            }
        }

        private void Main_Closing(object sender, CancelEventArgs e)
        {
            flaga = false;
            Thread.Sleep(100);
        }
        //zdarzenie do wyswietlenia profilu 
        private void button_Click(object sender, RoutedEventArgs e)
        {
            Win_profil_pracownik.ID_pracownika = ID_pracownika;
            var win = new Win_profil_pracownik();
            win.Show();
        }
        //metoda uruchamiajaca zegar zadania
        private void Rozpocznij_Click(object sender, RoutedEventArgs e)
        {
            if (txtIDZad.Text == "") return;
            timerZadania.Start();
            zadanieDataGrid.IsHitTestVisible = false;
            Rozpocznij.IsEnabled = false;
            Wstrzymaj.IsEnabled = true;

        }
        //metoda pauzujaca zegar zadania
        private void Wstrzymaj_Click(object sender, RoutedEventArgs e)
        {
            if (txtIDZad.Text == "") return;
            zadanieDataGrid.IsHitTestVisible = true;
            Rozpocznij.IsEnabled = true;
            Wstrzymaj.IsEnabled = false;
            timeOfZadanie = timeOfZadanie + timerZadania.Elapsed;
            timerZadania.Reset();
            using (var db = new BazaEntities())
            {
                update.Zegar = timeOfZadanie;
                db.Lista_Zadan.Attach(update);
                db.Entry(update).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception)
                {
                    MessageBox.Show("aaaa");
                }
            }
        }
        //metoda zatrzymajaca zegar zadania
        private void Zakoncz_Click(object sender, RoutedEventArgs e)
        {
            Wstrzymaj_Click(sender, e);
            if (txtIDZad.Text == "") return;
            using (var db = new BazaEntities())
            {
                var id = decimal.Parse(txtIDZad.Text);
                var wyszukaj = from p in db.Zadanie where p.ID_zadania == id select p;
                var dane = wyszukaj.FirstOrDefault();

                dane.Status_ID_statusu = 2;
                dane.Data_zakonczenia = DateTime.Today;
                
                db.Zadanie.Attach(dane);
                db.Entry(dane).State = EntityState.Modified;
                //================== MERYX - ZDANIA STAŁE!

                if (dane.Typ_Zadania_ID_typu_zadania == 3)
                {
                    decimal id2 = 0;

                    try
                    {
                        var wyszukaj2 = (from p in db.Zadanie select p.ID_zadania).Max();
                        id2 = wyszukaj2;
                    }
                    catch (Exception)
                    {
                        //
                    }
                    id2++;

                    var data_trwania = dane.Data_Deadline - dane.Data_Dodania;

                    var dodaj = new Zadanie
                    {
                        ID_zadania = id2,
                        Data_Deadline = DateTime.Today + data_trwania,
                        Status_ID_statusu = 1,
                        Data_Dodania = DateTime.Today,
                        Priorytet = dane.Priorytet,
                        Typ_Zadania_ID_typu_zadania = dane.Typ_Zadania_ID_typu_zadania,
                        Pracownicy_ID_pracownika = dane.Pracownicy_ID_pracownika,
                        Opis = dane.Opis,

                    };
                    var wyszukaj3 = from p in db.Lista_Zadan where p.Zadanie_ID_zadania == dane.ID_zadania select p;
                    decimal id3 = 0;

                    try
                    {
                        var wyszukaj5 = (from p in db.Lista_Zadan select p.ID_listy).Max();
                        id3 = wyszukaj5;
                    }
                    catch (Exception)
                    {
                        //
                    }

                    foreach (var dane2 in wyszukaj3)
                    {
                        id3++;
                        var dodaj2 = new Lista_Zadan
                        {
                            ID_listy = id3,
                            Zadanie_ID_zadania = id2,
                            Zegar = TimeSpan.Parse("0"),
                            Pracownicy_ID_pracownika = dane2.Pracownicy_ID_pracownika
                        };
                        db.Lista_Zadan.Add(dodaj2);
                    }

                    db.Zadanie.Add(dodaj);
                }

                //=============================================
                try
                {
                    db.SaveChanges();
                }
                catch (Exception)
                {
                    MessageBox.Show("Problem z połączeniem z baza !");
                    //throw;
                }
            }

            txtNazw.Text = "";
            txtImie.Text = "";
            txtOpis.Text = "";
            txtTypZad.Text = "";
            txtDataDod.Text = "";
            txtDataDead.Text = "";
            txtStatus.Text = "";
            txtIDZad.Text = "";
            txtIDPrac.Text = "";
            txtPriorytet.Text = "";
            txtTimer.Text = "000:00:00";
            ladowanie_zadan();
            LadowanieRaporty();
        }
        //pusta
        private void RaportcomboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        //metoda wczytujaca opis raportu
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if (RaportcomboBox.SelectedIndex > -1)
            {
                using (var db = new BazaEntities())
                {
                    var IDzad = Convert.ToDecimal(RaportcomboBox.Text);
                    var wyszukaj = from p in db.Zadanie where p.ID_zadania == IDzad select p;
                    var wyszukajka = wyszukaj.FirstOrDefault<Zadanie>();
                    opisBox.Text = wyszukajka.Opis;
                }
            }
        }
        //metoda wysylajaca raport
        private void Raportbutton1_Click(object sender, RoutedEventArgs e)
        {
            if (RaportcomboBox.Text == "" || RaporttextBox.Text == "")
                MessageBox.Show("Należy uzupełnić wszystkie dane do rapotu (ID Zadania i treść raportu) !");
            else
            {
                using (var db = new BazaEntities())
                {
                    var wyciag1 = from p in db.Raport select p.ID_raportu;
                    var listaID = wyciag1.ToList();
                    var id1 = Convert.ToDecimal(ID_pracownika);
                    var id2 = Convert.ToDecimal(RaportcomboBox.Text);

                    decimal id_oceny = 0;
                    try
                    {
                        id_oceny = (from p in db.Oceny select p.ID_Oceny).Max();
                    }
                    catch (Exception)
                    {

                        // throw;
                    }


                    id_oceny++;
                    var lista =
                        (from p in db.Lista_Zadan
                         where p.Pracownicy_ID_pracownika == id1 && p.Zadanie_ID_zadania == id2
                         select p.ID_listy).FirstOrDefault();
                    var dodaj = new Oceny
                    {
                        ID_Oceny = id_oceny,
                        Lista_Zadan_ID_listy = lista,
                        Typ_Oceny_ID_typu_oceny = 1
                    };
                    db.Oceny.Add(dodaj);
                    Raport nowyRaport;
                    try
                    {
                        nowyRaport = new Raport
                        {
                            ID_raportu = listaID.Last() + 1,
                            Data_utworzenia = DateTime.Now,
                            Opis = RaporttextBox.Text,
                            Pracownicy_ID_pracownika = id1,
                            Zadanie_ID_zadania = id2
                        };
                    }
                    catch (Exception)
                    {

                        nowyRaport = new Raport
                        {
                            ID_raportu = 0,
                            Data_utworzenia = DateTime.Now,
                            Opis = RaporttextBox.Text,
                            Pracownicy_ID_pracownika = id1,
                            Zadanie_ID_zadania = id2
                        };
                    }

                    db.Raport.Add(nowyRaport);
                    try
                    {
                        db.SaveChanges();
                        MessageBox.Show("Pomyślnie przesłano raport");
                        RaporttextBox.Text = "";
                        opisBox.Text = "";
                    }
                    catch (Exception)
                    {
                        db.Dispose();
                        MessageBox.Show("Taki raport już istnieje");
                    }
                }
            }
        }
        //metoda wysylajaca notatke
        private void Notatka_B_Click(object sender, RoutedEventArgs e)
        {
            if (txtIDZad.Text == "") return;
            Win_Notatka.ID_pracownika = ID_pracownika;
            Win_Notatka.ID_zadania = int.Parse(txtIDZad.Text);
            var win = new Win_Notatka(); //Administrator
            win.Show();
        }
        private const double RequiredWidth = 1264;

        private const double RequiredHeight = 669;

        private void Okno_pracownika_name_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var c = new double[2];
            c[0] = ActualWidth / RequiredWidth;
            c[1] = ActualHeight / RequiredHeight;
            var d = c.Min();

            skala.ScaleX = d;
            skala.ScaleY = d;
        }

        private void Okno_pracownika_name_StateChanged(object sender, EventArgs e)
        {
            var c = new double[2];
            c[0] = ActualWidth / RequiredWidth;
            c[1] = ActualHeight / RequiredHeight;
            var d = c.Min();

            skala.ScaleX = d;
            skala.ScaleY = d;
        }

        private void logout_Click(object sender, RoutedEventArgs e)
        {
            var Win = new Win_logowanie();
            Win.Show();
            this.Close();
        }
    }
}

/*
         using (var db = new BazaEntities())//laczymy sie z baza
         {
             //Dodanie wiersza na przykladzie encji STANOWISKO
             var dodanie = new Stanowisko { ID_Stanowiska = 3, Nazwa = "AAA" }; // definiujemy wiersz


             db.Stanowisko.Add(dodanie); //Dodajemy wiersz do tabeli

             try // try aby nie zdarzyla sie sytuacja ze dodajemy wiersze ktore juz istnieja
             {
                 db.SaveChanges(); //Zapisuje zmiany w bazie dopiero teraz dane zostana zapisae w bazie
             }
             catch (Exception)
             {
                 db.Dispose();//Odrzucamy zmiany i rozlaczamy sie z baza
                 MessageBox.Show("Dane już znajduja sie w bazie");
             }

         }

         using (var db = new BazaEntities())//laczymy sie z baza
         {
             //Dodanie wiersza na przykladzie encji STANOWISKO
             var dodanie = new Stanowisko { ID_Stanowiska = 4, Nazwa = "Administrator" }; // definiujemy wiersz
             db.Stanowisko.Add(dodanie); //Dodajemy wiersz do tabeli

             try // dodanie damy w try aby nie zdarzyla sie sytuacja ze dajemy wiersze ktore juz istnieja
             {
                 db.SaveChanges(); //Zapisuje zmiany w bazie dopiero teraz dane zostana zapisae w bazie
             }
             catch (Exception)
             {
                 db.Dispose();//Odrzucamy zmiany i rozlaczamy sie z baza
                 MessageBox.Show("Dane już znajduja sie w bazie");
             }

         }


         using (var db = new BazaEntities())//laczymy sie z baza
         {
             //Wyciaganie informacji z bazy
             var wyszukaj = db.Stanowisko.Where(c => c.ID_Stanowiska == 3); // szukamy w encji stanowiska o nr3
             var dane = wyszukaj.FirstOrDefault<Stanowisko>(); //z wynikow jakie mamy wybieramy 1 wartosc
             Pole.Text = dane.ID_Stanowiska.ToString() + " " + dane.Nazwa.ToString();

             //Zmiana wartosci wiersza (Update)
             var update = new Stanowisko { ID_Stanowiska = 1, Nazwa = "CCC" };//Zamiast RRR mozna wpisac inna wartosc
             db.Stanowisko.Attach(update);
             db.SaveChanges();

             //Ponownie wyswietlimy zmiany
             wyszukaj = db.Stanowisko.Where(c => c.ID_Stanowiska == 1); // szukamy w encji stanowiska o nr1
             dane = wyszukaj.FirstOrDefault<Stanowisko>(); //z wynikow jakie mamy wybieramy 1 wartosc
             Pole.Text += "\n" + dane.ID_Stanowiska.ToString() + " " + dane.Nazwa.ToString();


             //Usuwanie Najpierw szukamy goscia do usuwania a potem go usuwamy
             wyszukaj = db.Stanowisko.Where(c => c.ID_Stanowiska == 4); // szukamy w encji stanowiska o nr4
             dane = wyszukaj.FirstOrDefault<Stanowisko>(); //z wynikow jakie mamy wybieramy 1 wartosc
             db.Stanowisko.Remove(dane);
             db.SaveChanges();

             //Ponownie wyswietlimy zmiany
             wyszukaj = db.Stanowisko.Where(c => c.ID_Stanowiska == 4); // szukamy w encji stanowiska o nr4
             dane = wyszukaj.FirstOrDefault<Stanowisko>(); //z wynikow jakie mamy wybieramy 1 wartosc
             if (dane == null) Pole.Text += "\nBrak danych"; // _dane zwracaja wartosc null jesli nie ma tej osoby

         }*/


