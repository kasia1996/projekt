﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Objects;
using System.Data.EntityClient;
using System.Data;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System_zarzadania_zadaniami.Okna;

namespace System_zarzadania_zadaniami
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml 
    /// </summary>
    public partial class MainWindow3 : Window
    {

        //===========================================================
        Thread _watek_czatu;
        private bool flaga = true;
        private bool flaga2;
        Stopwatch _zegar_watku_czatu = new Stopwatch();
        private List<Czat> _dane_czatu;
        //===============================================  Czat


       

        private int ID_pracownika = Win_logowanie.IdPracownika;//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        public static int ID_pracownika_edycja = 0;

        public MainWindow3()
        {
           
            InitializeComponent();
            //Wiadomosc.MaxLength = 254;
            _watek_czatu = new Thread(Procedura_watku_czatu);
            _watek_czatu.Start();
            LadowanieRaportow();
            //czatDataGrid.Visibility = Visibility.Hidden;
            //Wiadomosc.Visibility = Visibility.Hidden;
            //Wyslij.Visibility = Visibility.Hidden;
        }

       
        // Nieuzywana metoda
        private void Odswiezenie_danych_Czatu()
        {
            using (var db = new BazaEntities())//laczymy sie z baza
            {
                var wyszukaj = from p in db.Czat orderby p.Data_wyslania select p;

                foreach (var dane in wyszukaj)
                {
                    var d = dane.Pracownicy.Imie;
                    d = dane.Pracownicy.Nazwisko;
                }
                var dane2 = wyszukaj.ToList();
                _dane_czatu = dane2;


                //czatDataGrid.ItemsSource = Filtrowanie_Czatu(dane2);
            }
        }

        //Watek, ktory co pewien czas odswieza datagrid i dodaje nowe wiadomosci
        private void Procedura_watku_czatu()
        {
            while (flaga)
            {

                if (flaga2)
                {
                    using (var db = new BazaEntities())//laczymy sie z baza
                    {
                        IList wybrany = null;
                        //zadanieDataGrid.Dispatcher.Invoke(new Action(() =>
                        //{

                        //    wybrany = zadanieDataGrid.SelectedItems;
                        //}));
                        var zadanie = (Zadanie)wybrany[0];
                        var ID_zadania = (int)zadanie.ID_zadania;

                        decimal wyszukaj = 0;
                        try
                        {
                            wyszukaj = (from p in db.Czat select p.ID_wiadomosci).Max();
                        }
                        catch (Exception)
                        {

                        }
                        Czat dodanie = null;
                        //Wiadomosc.Dispatcher.Invoke(new Action(() =>
                        //{
                        //    dodanie = new Czat()
                        //    {
                        //        ID_wiadomosci = ((int)wyszukaj) + 1,
                        //        Data_wyslania = DateTime.Now,
                        //        Tresc = Wiadomosc.Text,
                        //        Zadanie_ID_zadania = ID_zadania,
                        //        Pracownicy_ID_pracownika = ID_pracownika,

                        //    };
                        //    Wiadomosc.Text = "";
                        //}));

                        db.Czat.Add(dodanie);

                        try
                        {
                            db.SaveChanges();
                        }
                        catch (Exception)
                        {
                            db.Dispose();

                        }
                        //czatDataGrid.Dispatcher.Invoke(new Action(() =>
                        //{
                        //    Odswiezenie_danych_Czatu();
                        //}));
                        flaga2 = false;
                    }
                }


                if (_zegar_watku_czatu.Elapsed.Seconds == 10)
                {
                    _zegar_watku_czatu.Restart();
                    //czatDataGrid.Dispatcher.Invoke(new Action(() =>
                    //{
                    //    using (var db = new BazaEntities())//laczymy sie z baza
                    //    {
                    //        var wyszukaj = from p in db.Czat orderby p.Data_wyslania select p;

                    //        foreach (var dane in wyszukaj)
                    //        {
                    //            var d = dane.Pracownicy.Imie;
                    //            d = dane.Pracownicy.Nazwisko;
                    //        }
                    //        var dane2 = wyszukaj.ToList();
                    //        _dane_czatu = dane2;

                    //        czatDataGrid.ItemsSource = Filtrowanie_Czatu(dane2);
                    //    }
                    //}));
                    ladowanie_zadan();

                }
            }
        }

        //public ICollectionView Filtrowanie_Czatu(List<Czat> Czat)
        //{
        //    if (zadanieDataGrid.SelectedIndex == -1) return null;
        //    IList wybrany = zadanieDataGrid.SelectedItems;
        //    var zadanie = (Zadanie)wybrany[0];
        //    var ID_zadania = (int)zadanie.ID_zadania;
        //    var filtr = new Predicate<object>(i => ((Czat)i).Zadanie_ID_zadania == ID_zadania);

        //    var dane2 = Czat;
        //    var itemSourceList = new CollectionViewSource() { Source = dane2 };
        //    var itemlist = itemSourceList.View;
        //    itemlist.Filter = filtr;

        //    return itemlist;
        //}

            //Ładuje wszystkie raporty do grida
        private void LadowanieRaportow()
        {
            using (var db = new BazaEntities())
            {
                var wyszukaj = from p in db.Pracownicy where p.Stanowisko_ID_Stanowiska == 2 select p;
                foreach (var kier in wyszukaj)
                {
                    comboBox.Items.Add(kier.ID_pracownika + " " + kier.Imie + " " + kier.Nazwisko);
                }
            }

        }

        //Ładuje wszystkich pracownikow do grida
        private void LadowanieDanychPracowikow()
        {
            using (var db = new BazaEntities())//laczymy sie z baza
            {
                var wyszukaj = from p in db.Pracownicy orderby p.ID_pracownika select p;

                foreach (var dane in wyszukaj)
                {
                    var d = dane.Stanowisko.Nazwa;
                    try
                    {
                        var d2 = dane.Pracownicy2.Nazwisko;
                        var d3 = dane.Pracownicy2.Imie;
                    }
                    catch (Exception)
                    {

                        //
                    }

                }

                Pracownicy_DataGrid.ItemsSource = wyszukaj.ToList();
            }
        }

        //Ładuje wszystkie wiadomosci do grida
        private void LadowanieDanychWiadomosci()
        {
            using (var db = new BazaEntities())//laczymy sie z baza
            {
                var wyszukaj = from p in db.Zadanie from c in db.Lista_Zadan where p.ID_zadania == c.Zadanie_ID_zadania && c.Pracownicy_ID_pracownika == ID_pracownika && p.Status_ID_statusu != 3 select p;

                foreach (var dane in wyszukaj)
                {
                    var d = dane.Pracownicy.Nazwisko;
                }

                //zadanieDataGrid.ItemsSource = wyszukaj.ToList();
            }
            System.Windows.Data.CollectionViewSource czatViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("czatViewSource")));

            using (var db = new BazaEntities()) //laczymy sie z baza
            {
                var wyszukaj = from p in db.Czat orderby p.Data_wyslania select p;

                foreach (var dane in wyszukaj)
                {
                    var d = dane.Pracownicy.Imie;
                    d = dane.Pracownicy.Nazwisko;
                }
                var dane2 = wyszukaj.ToList();
                _dane_czatu = dane2;
            }
            _zegar_watku_czatu.Start();
        }

        //metoda do wypisywania nazwy zakładki w ktorej sie znajdujemy
        private void Tab_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var _nazwa = Tab.SelectedItem;
            Text_tab.Text = ((TabItem)_nazwa).Header.ToString();
        }

        //metoda do wysylania wiadomosci
        private void Wyslij_Click(object sender, RoutedEventArgs e)
        {
            flaga2 = true;
        }

        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Wyslij_Click(null, null);
            }
        }

        //private void zadanieDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    czatDataGrid.ItemsSource = Filtrowanie_Czatu(_dane_czatu);
        //    if (zadanieDataGrid.SelectedIndex != -1)
        //    {
        //        czatDataGrid.Visibility = Visibility.Visible;
        //        Wiadomosc.Visibility = Visibility.Visible;
        //        Wyslij.Visibility = Visibility.Visible;
        //    }
        //    else
        //    {
        //        czatDataGrid.Visibility = Visibility.Hidden;
        //        Wiadomosc.Visibility = Visibility.Hidden;
        //        Wyslij.Visibility = Visibility.Hidden;
        //    }
        //}


        private void Main_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            flaga = false;
            Thread.Sleep(100);
        }

        private void Main_Loaded(object sender, RoutedEventArgs e)
        {

            ladowanie_zadan();
            LadowanieDanychWiadomosci();
            LadowanieDanychPracowikow();
        }

        //zdarzenie do wyswietlenia profilu 
        private void button_Click_1(object sender, RoutedEventArgs e)
        {
            Win_profil_admin.ID_pracownika = ID_pracownika;
            var win = new Win_profil_admin(); //Administrator

            win.Show();
        }

        //Ładuje wszystkie zadania do grida
        private void ladowanie_zadan()
        {
            using (var db = new BazaEntities())
            {

                var wyszukaj = from p in db.Zadanie where p.Pracownicy_ID_pracownika == ID_pracownika select p;

                foreach (var _dane in wyszukaj)
                {
                    var d = _dane.Status.Nazwa;
                    d = _dane.Typ_Zadania.nazwa;
                    d = _dane.Pracownicy.Nazwisko;
                    d = _dane.Pracownicy.Imie;


                }
                var dane = wyszukaj.ToList();
                try
                {
                    zadanieDataGrid2.Dispatcher.Invoke(new Action(() =>
                    {
                        zadanieDataGrid2.ItemsSource = dane;
                        zadanieDataGrid2.IsHitTestVisible = zadanieDataGrid2.HasItems;
                    }));
                }
                catch (Exception)
                {
                    //
                }

            }
        }

        //metoda do aktywacji pracownika
        private void Aktywuj_pracownika_Click(object sender, RoutedEventArgs e)
        {
            if (Pracownicy_DataGrid.SelectedIndex != -1)
            {
                var pracownik = (Pracownicy)Pracownicy_DataGrid.SelectedItem;
                if (pracownik.Stanowisko_ID_Stanowiska != 0)
                {
                    MessageBox.Show("Osoba jest już aktywna!");
                    return;
                }


                using (var db = new BazaEntities())
                {
                    var wyszukaj = from p in db.Pracownicy where p.ID_pracownika == pracownik.ID_pracownika select p;
                    var pracownik2 = wyszukaj.FirstOrDefault();
                    pracownik2.Stanowisko_ID_Stanowiska = 1;
                    db.Pracownicy.Attach(pracownik2);
                    db.Entry(pracownik2).State = EntityState.Modified;
                    try
                    {
                        db.SaveChanges();
                        LadowanieDanychPracowikow();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Problem z Bazą");
                    }
                }
            }
            else
            {
                MessageBox.Show("Proszę wybrać osobę do aktywacji!");
            }
        }

        //Metoda do dodawania pracownika
        private void DodajPracownikaButton_Click(object sender, RoutedEventArgs e)
        {
            var win = new Win_dodaj_pracownika();
            win.Show();
        }

        //Metoda do edycji pracownika
        private void Edytuj_Click(object sender, RoutedEventArgs e)
        {
            if (Pracownicy_DataGrid.SelectedIndex != -1)
            {
                var pracownik = (Pracownicy)Pracownicy_DataGrid.SelectedItem;
                ID_pracownika_edycja = (int)pracownik.ID_pracownika;
                var win = new Win_edytuj_pracownika();
                win.Show();
            }
            else
            {
                MessageBox.Show("Proszę wybrać osobę do edycji!");
            }
        }

        //Ładuje wszystkie informacje o zadaniu do grida
        private void zadanieDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (zadanieDataGrid2.SelectedIndex >= 0)
            {
                var getRow = (Zadanie)zadanieDataGrid2.CurrentCell.Item;
                txtNazw.Text = getRow.Pracownicy.Nazwisko;
                txtImie.Text = getRow.Pracownicy.Imie;
                txtOpis.Text = getRow.Opis;
                txtTypZad.Text = getRow.Typ_Zadania.nazwa;
                txtDataDod.Text = getRow.Data_Dodania.ToString();
                txtDataDead.Text = getRow.Data_Deadline.ToString();
                txtStatus.Text = getRow.Status.Nazwa;
                txtIDZad.Text = getRow.ID_zadania.ToString();
                txtIDPrac.Text = getRow.Pracownicy_ID_pracownika.ToString();

                using (var db = new BazaEntities())
                {
                    var wyszukaj = from p in db.Lista_Zadan
                                   where p.Zadanie_ID_zadania == getRow.ID_zadania
                                   select p;

                    Osob_przydz.Text = "";

                    foreach (var dane in wyszukaj)
                    {
                        Osob_przydz.Text += dane.Pracownicy.Imie + " " + dane.Pracownicy.Nazwisko + "\n";
                    }

                }

            }
        }

        //Drukowanie raportu
        private void button2_Click(object sender, RoutedEventArgs e)
        {
            PrintDialog pDialog = new PrintDialog();

            if (pDialog.ShowDialog() == true)
            {
                pDialog.PrintVisual(textBox, "Raport:"); //to sie pomyśli potem kiedyś, pewnie przydałaby sie jakaś pro biblioteka

            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            comboBox.Items.Clear();
            using (var db = new BazaEntities())
            {
                var wyszukaj = from p in db.Pracownicy where p.Stanowisko_ID_Stanowiska == 2 select p;
                foreach (var kier in wyszukaj)
                {
                    comboBox.Items.Add(kier.ID_pracownika + " " + kier.Imie + " " + kier.Nazwisko);
                }
            }
        }

        private void button_Copy1_Click(object sender, RoutedEventArgs e)
        {
            comboBox1.Items.Clear();
            var zmienna = Convert.ToDecimal(comboBox.Text.Split(' ').First());
            using (var db = new BazaEntities())
            {
                var wyszukaj = from p in db.Zadanie where p.Pracownicy_ID_pracownika == zmienna select p;
                foreach (var kier in wyszukaj)
                {
                    comboBox1.Items.Add(kier.ID_zadania + " " + kier.Opis);
                }
            }
        }

        private void button_Copy2_Click(object sender, RoutedEventArgs e)
        {
            comboBox2.Items.Clear();
            var zmienna = Convert.ToDecimal(comboBox.Text.Split(' ').First());
            var zmienna2 = Convert.ToDecimal(comboBox1.Text.Split(' ').First());
            using (var db = new BazaEntities())
            {
                var wyszukaj = from p in db.Raport where p.Pracownicy_ID_pracownika == zmienna && p.Zadanie_ID_zadania == zmienna2 select p;
                foreach (var kier in wyszukaj)
                {
                    comboBox2.Items.Add(kier.ID_raportu + " " + Convert.ToString(kier.Data_utworzenia));
                }
            }
        }

        private void button_Copy_Click(object sender, RoutedEventArgs e)
        {
            textBox.Clear();
            var zmienna = Convert.ToDecimal(comboBox.Text.Split(' ').First());
            var zmienna2 = Convert.ToDecimal(comboBox1.Text.Split(' ').First());
            var zmienna3 = Convert.ToDecimal(comboBox2.Text.Split(' ').First());
            using (var db = new BazaEntities())
            {
                var wyszukaj = from p in db.Raport where p.Pracownicy_ID_pracownika == zmienna && p.Zadanie_ID_zadania == zmienna2 && p.ID_raportu == zmienna3 select p;
                var dane = wyszukaj.FirstOrDefault<Raport>();
                textBox.Text = dane.Opis;
            }
        }

        //Usuwanie raportu
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            var ID = Convert.ToDecimal(comboBox2.Text.Split(' ').First());
            using (var db = new BazaEntities())
            {
                var cos = from p in db.Raport where p.ID_raportu == ID select p;
                foreach (var row in cos)
                {
                    db.Raport.Remove(row);
                }
                db.SaveChanges();
                MessageBox.Show("Raport pomyślnie usunięty!");
                comboBox2.Items.Clear();
            }
        }

        //Wybor kierownika do raportu
        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboBox.Items.Count == 0) return;
            comboBox1.Items.Clear();
            comboBox2.Items.Clear();
            textBox.Clear();

            var wybrany = (ComboBox)sender;
      
            var zmienna = Convert.ToDecimal(wybrany.SelectedItem.ToString().Split(' ').First());
            using (var db = new BazaEntities())
            {
                var wyszukaj = from p in db.Zadanie where p.Pracownicy_ID_pracownika == zmienna select p;
                foreach (var kier in wyszukaj)
                {
                    comboBox1.Items.Add(kier.ID_zadania + " " + kier.Opis);
                }
            }
        }

        //Wybor zadania do raportu
        private void comboBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboBox1.Items.Count == 0) return;
            comboBox2.Items.Clear();
            textBox.Clear();
 
            var zmienna = Convert.ToDecimal(comboBox.Text.Split(' ').First());
            var zmienna2 = Convert.ToDecimal((sender as ComboBox).SelectedItem.ToString().Split(' ').First());
            using (var db = new BazaEntities())
            {
                var wyszukaj = from p in db.Raport where p.Pracownicy_ID_pracownika == zmienna && p.Zadanie_ID_zadania == zmienna2 select p;
                foreach (var kier in wyszukaj)
                {
                    comboBox2.Items.Add(kier.ID_raportu + " " + Convert.ToString(kier.Data_utworzenia));
                }
            }
        }

        //Wybor raportu do raportu
        private void comboBox2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboBox2.Items.Count == 0) return;
            textBox.Clear();
            var zmienna = Convert.ToDecimal(comboBox.Text.Split(' ').First());
            var zmienna2 = Convert.ToDecimal(comboBox1.Text.Split(' ').First());
            var zmienna3 = Convert.ToDecimal((sender as ComboBox).SelectedItem.ToString().Split(' ').First());
            using (var db = new BazaEntities())
            {
                var wyszukaj = from p in db.Raport where p.Pracownicy_ID_pracownika == zmienna && p.Zadanie_ID_zadania == zmienna2 && p.ID_raportu == zmienna3 select p;
                var dane = wyszukaj.FirstOrDefault<Raport>();
                textBox.Text = dane.Opis;
            }
        }

        //motoda do dodania zadania
        private void button1_Copy_Click(object sender, RoutedEventArgs e)
        {
            var win = new Win_dodaj_zadanie();
            win.Show();
        }

        //metoda do edycji zadania
        private void button2_Copy_Click(object sender, RoutedEventArgs e)
        {
            if (txtIDZad.Text == "") return;
            Win_edytuj_zadanie.IDZadania = int.Parse(txtIDZad.Text);
            var Win = new Win_edytuj_zadanie();
            Win.Show();
        }
        // Funkcja skalowania 4 w doł
        private const double RequiredWidth = 1264;

        private const double RequiredHeight = 669;

        private void Main_StateChanged(object sender, EventArgs e)
        {
            var c = new double[2];
            c[0] = ActualWidth / RequiredWidth;
            c[1] = ActualHeight / RequiredHeight;
            var d = c.Min();

            skala.ScaleX = d;
            skala.ScaleY = d;
        }

        private void Main_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var c = new double[2];
            c[0] = ActualWidth / RequiredWidth;
            c[1] = ActualHeight / RequiredHeight;
            var d = c.Min();

            skala.ScaleX = d;
            skala.ScaleY = d;
        }

       
        private void logout_Click(object sender, RoutedEventArgs e)
        {
            var Win = new Win_logowanie();
            Win.Show();
            this.Close();
        }
    }
}
/*
 * //SPRAWDZANIE DOMINIKI REPOOOOO
         using (var db = new BazaEntities())//laczymy sie z baza
         {
             //Dodanie wiersza na przykladzie encji STANOWISKO
             var dodanie = new Stanowisko { ID_Stanowiska = 3, Nazwa = "AAA" }; // definiujemy wiersz


             db.Stanowisko.Add(dodanie); //Dodajemy wiersz do tabeli

             try // try aby nie zdarzyla sie sytuacja ze dodajemy wiersze ktore juz istnieja
             {
                 db.SaveChanges(); //Zapisuje zmiany w bazie dopiero teraz dane zostana zapisae w bazie
             }
             catch (Exception)
             {
                 db.Dispose();//Odrzucamy zmiany i rozlaczamy sie z baza
                 MessageBox.Show("Dane już znajduja sie w bazie");
             }

         }

         using (var db = new BazaEntities())//laczymy sie z baza
         {
             //Dodanie wiersza na przykladzie encji STANOWISKO
             var dodanie = new Stanowisko { ID_Stanowiska = 4, Nazwa = "Administrator" }; // definiujemy wiersz
             db.Stanowisko.Add(dodanie); //Dodajemy wiersz do tabeli

             try // dodanie damy w try aby nie zdarzyla sie sytuacja ze dajemy wiersze ktore juz istnieja
             {
                 db.SaveChanges(); //Zapisuje zmiany w bazie dopiero teraz dane zostana zapisae w bazie
             }
             catch (Exception)
             {
                 db.Dispose();//Odrzucamy zmiany i rozlaczamy sie z baza
                 MessageBox.Show("Dane już znajduja sie w bazie");
             }

         }


         using (var db = new BazaEntities())//laczymy sie z baza
         {
             //Wyciaganie informacji z bazy
             var wyszukaj = db.Stanowisko.Where(c => c.ID_Stanowiska == 3); // szukamy w encji stanowiska o nr3
             var dane = wyszukaj.FirstOrDefault<Stanowisko>(); //z wynikow jakie mamy wybieramy 1 wartosc
             Pole.Text = dane.ID_Stanowiska.ToString() + " " + dane.Nazwa.ToString();

             //Zmiana wartosci wiersza (Update)
             var update = new Stanowisko { ID_Stanowiska = 1, Nazwa = "CCC" };//Zamiast RRR mozna wpisac inna wartosc
             db.Stanowisko.Attach(update);
             db.SaveChanges();

             //Ponownie wyswietlimy zmiany
             wyszukaj = db.Stanowisko.Where(c => c.ID_Stanowiska == 1); // szukamy w encji stanowiska o nr1
             dane = wyszukaj.FirstOrDefault<Stanowisko>(); //z wynikow jakie mamy wybieramy 1 wartosc
             Pole.Text += "\n" + dane.ID_Stanowiska.ToString() + " " + dane.Nazwa.ToString();


             //Usuwanie Najpierw szukamy goscia do usuwania a potem go usuwamy
             wyszukaj = db.Stanowisko.Where(c => c.ID_Stanowiska == 4); // szukamy w encji stanowiska o nr4
             dane = wyszukaj.FirstOrDefault<Stanowisko>(); //z wynikow jakie mamy wybieramy 1 wartosc
             db.Stanowisko.Remove(dane);
             db.SaveChanges();

             //Ponownie wyswietlimy zmiany
             wyszukaj = db.Stanowisko.Where(c => c.ID_Stanowiska == 4); // szukamy w encji stanowiska o nr4
             dane = wyszukaj.FirstOrDefault<Stanowisko>(); //z wynikow jakie mamy wybieramy 1 wartosc
             if (dane == null) Pole.Text += "\nBrak danych"; // _dane zwracaja wartosc null jesli nie ma tej osoby

         }*/
