﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace System_zarzadania_zadaniami.Okna
{
    /// <summary>
    /// Interaction logic for Win_profil_kierownik.xaml
    /// </summary>
    public partial class Win_profil_kierownik : Window
    {


        public static int ID_pracownika = 1;
        public Win_profil_kierownik()
        {
            InitializeComponent();
        }
        private void LoadDanePracownika()
        {
            var IdPracownika = Win_logowanie.IdPracownika;
            //Test
            //IdPracownika = 2;
            using (var db = new BazaEntities())
            {
                var wyciag1 = from p in db.Stanowisko select p.Nazwa;
                var wyciag2 = from p in db.Pracownicy select p.Nazwisko;
                var wyciag3 = from r in db.Pracownicy select r.Imie;
                var kierownik = wyciag2.ToList().Zip(wyciag3.ToList(), (n, i) => n + " " + i);

                var wyszukaj = from p in db.Pracownicy where p.ID_pracownika == IdPracownika select p;
                var dane = wyszukaj.ToList();
                ImieLabel.Content = dane[0].Imie;
                NazwiskoLabel.Content = dane[0].Nazwisko;
                HasloLabel.Content = dane[0].haslo;
                DataUrodzeniaLabel.Content = dane[0].Data_urodzenia.ToShortDateString();
                DataZatrudnieniaLabel.Content = dane[0].Data_zatrudnienia.ToShortDateString();
                StanowiskoLabel.Content = dane[0].Stanowisko.Nazwa;
                if (dane[0].Pracownicy2 != null)
                    KierownikLabel.Content = dane[0].Pracownicy2.Nazwisko + " " + dane[0].Pracownicy2.Imie;
                else
                    KierownikLabel.Content = "";


            }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadDanePracownika();
        }

        private void Tab_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var _nazwa = Tab.SelectedItem;
            Text_tab.Text = ((TabItem)_nazwa).Header.ToString();
        }

        private void Urlop_DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }


        private const double RequiredWidth = 1264;

        private const double RequiredHeight = 669;

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var c = new double[2];
            c[0] = ActualWidth / RequiredWidth;
            c[1] = ActualHeight / RequiredHeight;
            var d = c.Min();

            skala.ScaleX = d;
            skala.ScaleY = d;
        }

        private void Window_StateChanged(object sender, EventArgs e)
        {
            var c = new double[2];
            c[0] = ActualWidth / RequiredWidth;
            c[1] = ActualHeight / RequiredHeight;
            var d = c.Min();

            skala.ScaleX = d;
            skala.ScaleY = d;
        }
    }
}
