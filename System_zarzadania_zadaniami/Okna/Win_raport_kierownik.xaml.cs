﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace System_zarzadania_zadaniami.Okna
{
    /// <summary>
    /// Interaction logic for Win_raport_kierownik.xaml
    /// </summary>
    public partial class Win_raport_kierownik : Window
    {
        private int ID_pracownika = Win_logowanie.IdPracownika;

        public Win_raport_kierownik()
        {
            InitializeComponent();
            using (var db = new BazaEntities())
            {
                var wyszukaj = from p in db.Zadanie where p.Pracownicy_ID_pracownika == ID_pracownika select p;
                foreach (var kier in wyszukaj)
                {
                    comboBox.Items.Add(kier.ID_zadania + " " + kier.Opis);
                }
            }
        }
        //nieuwzywane
        private void button_Click(object sender, RoutedEventArgs e)
        {
            comboBox1.Items.Clear();
            using (var db = new BazaEntities())
            {
                var item = comboBox.Text.Split(' ').First();
                var item2 = Convert.ToDecimal(item);


                var wyszukaj = from p in db.Pracownicy join a in db.Raport on p.ID_pracownika equals a.Pracownicy_ID_pracownika where a.Zadanie_ID_zadania == item2 && a.Pracownicy_ID_pracownika != ID_pracownika select p;

                foreach (var kier in wyszukaj)
                {
                    comboBox1.Items.Add(kier.ID_pracownika + " " + kier.Imie + " " + kier.Nazwisko);
                }
            }
        }
        //nieuwzywane
        private void button_Copy1_Click(object sender, RoutedEventArgs e)
        {
            comboBox.Items.Clear();
            using (var db = new BazaEntities())
            {
                var wyszukaj = from p in db.Zadanie where p.Pracownicy_ID_pracownika == ID_pracownika select p;
                foreach (var kier in wyszukaj)
                {
                    comboBox.Items.Add(kier.ID_zadania + " " + kier.Opis);
                }
            }
        }
        //nieuwzywane
        private void button_Copy_Click(object sender, RoutedEventArgs e)
        {
            textBox.Clear();

            var ID = Convert.ToDecimal(comboBox2.Text.Split(' ').First());

            using (var db = new BazaEntities())
            {
                var wyszukaj = from p in db.Raport where p.ID_raportu == ID select p;
                var dane = wyszukaj.FirstOrDefault<Raport>();
                textBox.Text = dane.Opis;

            }
        }
        //nieuwzywane
        private void button_Copy2_Click(object sender, RoutedEventArgs e)
        {
            var item = comboBox.Text.Split(' ').First();
            var item2 = Convert.ToDecimal(item);

            var item3 = comboBox1.Text.Split(' ').First();
            var item4 = Convert.ToDecimal(item3);

            using (var db = new BazaEntities())
            {
                var wyszukaj = from p in db.Raport where p.Pracownicy_ID_pracownika == item4 && p.Zadanie_ID_zadania == item2 select p;
                foreach (var kier in wyszukaj)
                {
                    comboBox2.Items.Add(kier.ID_raportu + " " + Convert.ToString(kier.Data_utworzenia));
                }

            }

        }

        //usuwanie raportu 
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            var ID = Convert.ToDecimal(comboBox2.Text.Split(' ').First());
            using (var db = new BazaEntities())
            {
                var cos = from p in db.Raport where p.ID_raportu == ID select p;
                foreach (var row in cos)
                {
                    db.Raport.Remove(row);
                }
                db.SaveChanges();
                MessageBox.Show("Raport pomyślnie usunięty!");
                comboBox2.Items.Clear();
            }
        }

        //Wybor zadania do raportu
        private void comboBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            if (comboBox.Items.Count == 0) return;
            comboBox1.Items.Clear();
            comboBox2.Items.Clear();
            textBox.Clear();
            using (var db = new BazaEntities())
            {
                var wybrany = (ComboBox)sender;
                var item = wybrany.SelectedItem.ToString().Split(' ').First();
                var item2 = Convert.ToDecimal(item);


                var wyszukaj = from p in db.Pracownicy join a in db.Raport on p.ID_pracownika equals a.Pracownicy_ID_pracownika where a.Zadanie_ID_zadania == item2 && a.Pracownicy_ID_pracownika != ID_pracownika select p;

                foreach (var kier in wyszukaj)
                {
                    comboBox1.Items.Add(kier.ID_pracownika + " " + kier.Imie + " " + kier.Nazwisko);
                }
            }
        }

        //Wybor pracownika do raportu
        private void comboBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboBox1.Items.Count == 0) return;
            comboBox2.Items.Clear();
            textBox.Clear();
            var item = comboBox.Text.Split(' ').First();
            var item2 = Convert.ToDecimal(item);

            // var item3 = comboBox1.Text.Split(' ').First();
            var item3 = (sender as ComboBox).SelectedItem.ToString().Split(' ').First();
            var item4 = Convert.ToDecimal(item3);

            using (var db = new BazaEntities())
            {
                var wyszukaj = from p in db.Raport where p.Pracownicy_ID_pracownika == item4 && p.Zadanie_ID_zadania == item2 select p;
                foreach (var kier in wyszukaj)
                {
                    comboBox2.Items.Add(kier.ID_raportu + " " + Convert.ToString(kier.Data_utworzenia));
                }

            }
        }

        //Wybor raportu do raportu
        private void comboBox2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboBox2.Items.Count == 0) return;
            textBox.Clear();

            //  var ID = Convert.ToDecimal(comboBox2.Text.Split(' ').First());
            var ID = Convert.ToDecimal((sender as ComboBox).SelectedItem.ToString().Split(' ').First());
            using (var db = new BazaEntities())
            {
                var wyszukaj = from p in db.Raport where p.ID_raportu == ID select p;
                var dane = wyszukaj.FirstOrDefault<Raport>();
                textBox.Text = dane.Opis;

            }
        }
    }
}
