﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace System_zarzadania_zadaniami.Okna
{
    /// <summary>
    /// Interaction logic for Win_Notatka.xaml
    /// </summary>
    public partial class Win_Notatka : Window
    {
        public static int ID_pracownika;
        public static int ID_zadania;
        public Win_Notatka()
        {
            InitializeComponent();
        }

        private void ladowanie_notatek()
        {
            using (var db = new BazaEntities())
            {
                var wyszukaj = from p in db.Notatka where p.Zadanie_ID_zadania == ID_zadania select p;

                foreach (var dane in wyszukaj)
                {
                    var d = dane.Pracownicy.Imie;
                    d = dane.Pracownicy.Nazwisko;
                }
                var wyszukaj2 = from p in db.Typ_Notatki select p;

                foreach (var dane in wyszukaj2)
                {
                    ComboTyp.Items.Add(dane.nazwa);
                    ComboTyp.SelectedIndex = 0;
                }
                notatkaDataGrid.ItemsSource = wyszukaj.ToList();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            System.Windows.Data.CollectionViewSource notatkaViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("notatkaViewSource")));
            // Load data by setting the CollectionViewSource.Source property:
            // notatkaViewSource.Source = [generic data source]
            ladowanie_notatek();
        }

        private void nowa_notatka_Click(object sender, RoutedEventArgs e)
        {
            nowa_notatka.IsEnabled = false;
            Add_notatka.IsEnabled = true;
            notatkaDataGrid.IsEnabled = false;
            Opis.Text = "";
            ComboTyp.SelectedIndex = 0;
            ComboTyp.Visibility = Visibility.Visible;
            label.Visibility = Visibility.Visible;
            Opis.IsHitTestVisible = true;

        }

        private void Add_notatka_Click(object sender, RoutedEventArgs e)
        {
            nowa_notatka.IsEnabled = true;
            Add_notatka.IsEnabled = false;
            notatkaDataGrid.IsEnabled = true;
            Opis.IsHitTestVisible = false;
            ComboTyp.Visibility = Visibility.Hidden;
            label.Visibility = Visibility.Hidden;
            using (var db = new BazaEntities())
            {

                var id = 0;
                try
                {
                    var wyszukaj = (from p in db.Notatka select p.ID_notatki).Max();
                    id = (int)wyszukaj;
                    id++;
                }
                catch (Exception)
                {

                    //
                }


                var dane = new Notatka
                {
                    Pracownicy_ID_pracownika = ID_pracownika,
                    Opis = Opis.Text,
                    Zadanie_ID_zadania = ID_zadania,
                    Typ_Notatki_ID_typ_notatki = (ComboTyp.SelectedIndex + 1),
                    ID_notatki = id
                };

                db.Notatka.Add(dane);

                try
                {
                    db.SaveChanges();
                    MessageBox.Show("Dodano Notatkę");
                }
                catch (Exception)
                {
                    MessageBox.Show("Problem z połączeniem z bazą!");
                }
                var id_p = 0;
                try
                {
                    var wyszukaj_id = (from p in db.Powiadomienia select p.ID_powiadomienia).Max();
                    id_p = (int)wyszukaj_id;
                    
                }
                catch (Exception)
                {

                    //
                }

                var wyszukaj2 = from q in db.Lista_Zadan where q.Zadanie_ID_zadania==ID_zadania orderby q.ID_listy select q;
                for (int i = 0; i < wyszukaj2.Count(); i++)
                {
                    var daneq = wyszukaj2.Skip(i).First();
                    Powiadomienia update = new Powiadomienia();
                    id_p++;
                    update.ID_powiadomienia = id_p;
                    update.Opis = "Notatka została dodana do zadania o ID: " + ID_zadania.ToString();
                    update.Lista_Zadan_ID_listy = daneq.ID_listy;
                    db.Powiadomienia.Add(update);
                    db.Entry(update).State = System.Data.EntityState.Added;
                }
                try
                {
                    db.SaveChanges();

                }
                catch (Exception)
                {
                    MessageBox.Show("Problem z połączeniem z bazą!");
                }
            }
            this.Close();
        }
        //wczytanie opisu notatki
        private void notatkaDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (notatkaDataGrid.Items.Count == 0) return;
            var wybrana = (Notatka) notatkaDataGrid.SelectedItem;

            Opis.Text = wybrana.Opis;
        }
    }
}
