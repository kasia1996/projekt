﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace System_zarzadania_zadaniami.Okna
{
    /// <summary>
    /// Interaction logic for Win_dodaj_pracownika.xaml
    /// </summary>
    public partial class Win_edytuj_pracownika : Window
    {
        private int ID_pracownika = 0;

        public Win_edytuj_pracownika() 
        {
            InitializeComponent();
            ID_pracownika = MainWindow3.ID_pracownika_edycja;
        }

        private void WinLoaded(object sender, RoutedEventArgs e)
        {
            using (var db = new BazaEntities())
            {
                var wyciag1 = from p in db.Stanowisko select p.Nazwa;
                StanowiskoComboBox.ItemsSource = wyciag1.ToList();
                var wyciag2 = from p in db.Pracownicy select p.Nazwisko;
                var wyciag3 = from r in db.Pracownicy select r.Imie;
                var kierownik = wyciag2.ToList().Zip(wyciag3.ToList(), (n, i) => n + " " + i);
                KierownikComboBox.Items.Add("");
                foreach (var kier in kierownik)
                {
                    KierownikComboBox.Items.Add(kier);
                }

                var wyszukaj = from p in db.Pracownicy where p.ID_pracownika == ID_pracownika select p;
                var dane = wyszukaj.ToList();
                ImieTextBox.Text = dane[0].Imie;
                NazwiskoTextBox.Text = dane[0].Nazwisko;
                HasloTextBox.Text = dane[0].haslo;
                DataUrodzeniaDatePicker.Text = dane[0].Data_urodzenia.ToShortDateString();
                DataZatrudnieniaDatePicker.Text = dane[0].Data_zatrudnienia.ToShortDateString();
                StanowiskoComboBox.Text = dane[0].Stanowisko.Nazwa;
                KierownikComboBox.Text = dane[0].Pracownicy2.Nazwisko + " " + dane[0].Pracownicy2.Imie;


            }
        }

        private void DodajPracownikaButton_Click(object sender, RoutedEventArgs e)
        {

            if (ImieTextBox.Text == "" ||
                NazwiskoTextBox.Text == "" ||
                HasloTextBox.Text == "" ||
                DataUrodzeniaDatePicker.Text == "" ||
                DataZatrudnieniaDatePicker.Text == "" ||
                StanowiskoComboBox.Text == "")
                MessageBox.Show("Prosze wypełnić wszystkie potrzebne dane");
            else
            {
                using (var db = new BazaEntities())
                {
                    var wyciag1 = from p in db.Pracownicy select p.ID_pracownika;
                    var listaID = wyciag1.ToList();
                    var wyciag2 = from p in db.Stanowisko where p.Nazwa == StanowiskoComboBox.Text select p.ID_Stanowiska;
                    var stanowiskoID = wyciag1.ToList();
                    var kierownikNazwisko = KierownikComboBox.Text.Split(' ').First();
                    var kierownikImie = KierownikComboBox.Text.Split(' ').Last();
                    var wyciag3 = from p in db.Pracownicy where p.Nazwisko == kierownikNazwisko && p.Imie == kierownikImie select p.ID_pracownika;
                    var kierownikID = wyciag3.ToList();
                    //var wyciag3 = from p in db.Pracownicy where p.Nazwisko == NazwiskoTextBox.Text
                    /*
                        public decimal ID_pracownika { get; set; }
                        public string haslo { get; set; }
                        public string Imie { get; set; }
                        public string Nazwisko { get; set; }
                        public System.DateTime Data_urodzenia { get; set; }
                        public System.DateTime Data_zatrudnienia { get; set; }
                        public Nullable<decimal> Pracownicy_ID_pracownika { get; set; }
                        public int Stanowisko_ID_Stanowiska { get; set; }
                        */
                    Pracownicy nowyPracownik;
                    if (kierownikID.FirstOrDefault() != 0)
                    {
                        nowyPracownik = new Pracownicy
                        {
                            ID_pracownika = ID_pracownika,
                            haslo = HasloTextBox.Text,
                            Imie = ImieTextBox.Text,
                            Nazwisko = NazwiskoTextBox.Text,
                            Data_urodzenia = DataUrodzeniaDatePicker.SelectedDate.Value,
                            Data_zatrudnienia = DataZatrudnieniaDatePicker.SelectedDate.Value,
                            Pracownicy_ID_pracownika = Decimal.ToInt32(kierownikID.FirstOrDefault()),
                            Stanowisko_ID_Stanowiska = Decimal.ToInt32(stanowiskoID.FirstOrDefault())

                        };
                    }
                    else
                    {
                        nowyPracownik = new Pracownicy
                        {
                            ID_pracownika = ID_pracownika,
                            haslo = HasloTextBox.Text,
                            Imie = ImieTextBox.Text,
                            Nazwisko = NazwiskoTextBox.Text,
                            Data_urodzenia = DataUrodzeniaDatePicker.SelectedDate.Value,
                            Data_zatrudnienia = DataZatrudnieniaDatePicker.SelectedDate.Value,
                            Stanowisko_ID_Stanowiska = Decimal.ToInt32(stanowiskoID.FirstOrDefault())

                        };
                    }
                    db.Pracownicy.Attach(nowyPracownik);
                    db.Entry(nowyPracownik).State = EntityState.Modified;
                    try
                    {
                        db.SaveChanges();
                        this.Close();
                    }
                    catch (Exception)
                    {
                        db.Dispose();
                        MessageBox.Show("Problem z baza!");
                    }
                }
            }

        }
    }
}

/*
         using (var db = new BazaEntities())//laczymy sie z baza
         {
             //Dodanie wiersza na przykladzie encji STANOWISKO
             var dodanie = new Stanowisko { ID_Stanowiska = 3, Nazwa = "AAA" }; // definiujemy wiersz


             db.Stanowisko.Add(dodanie); //Dodajemy wiersz do tabeli

             try // try aby nie zdarzyla sie sytuacja ze dodajemy wiersze ktore juz istnieja
             {
                 db.SaveChanges(); //Zapisuje zmiany w bazie dopiero teraz dane zostana zapisae w bazie
             }
             catch (Exception)
             {
                 db.Dispose();//Odrzucamy zmiany i rozlaczamy sie z baza
                 MessageBox.Show("Dane już znajduja sie w bazie");
             }

         }

         using (var db = new BazaEntities())//laczymy sie z baza
         {
             //Dodanie wiersza na przykladzie encji STANOWISKO
             var dodanie = new Stanowisko { ID_Stanowiska = 4, Nazwa = "Administrator" }; // definiujemy wiersz
             db.Stanowisko.Add(dodanie); //Dodajemy wiersz do tabeli

             try // dodanie damy w try aby nie zdarzyla sie sytuacja ze dajemy wiersze ktore juz istnieja
             {
                 db.SaveChanges(); //Zapisuje zmiany w bazie dopiero teraz dane zostana zapisae w bazie
             }
             catch (Exception)
             {
                 db.Dispose();//Odrzucamy zmiany i rozlaczamy sie z baza
                 MessageBox.Show("Dane już znajduja sie w bazie");
             }

         }


         using (var db = new BazaEntities())//laczymy sie z baza
         {
             //Wyciaganie informacji z bazy
             var wyszukaj = db.Stanowisko.Where(c => c.ID_Stanowiska == 3); // szukamy w encji stanowiska o nr3
             var dane = wyszukaj.FirstOrDefault<Stanowisko>(); //z wynikow jakie mamy wybieramy 1 wartosc
             Pole.Text = dane.ID_Stanowiska.ToString() + " " + dane.Nazwa.ToString();

             //Zmiana wartosci wiersza (Update)
             var update = new Stanowisko { ID_Stanowiska = 1, Nazwa = "CCC" };//Zamiast RRR mozna wpisac inna wartosc
             db.Stanowisko.Attach(update);
             db.SaveChanges();

             //Ponownie wyswietlimy zmiany
             wyszukaj = db.Stanowisko.Where(c => c.ID_Stanowiska == 1); // szukamy w encji stanowiska o nr1
             dane = wyszukaj.FirstOrDefault<Stanowisko>(); //z wynikow jakie mamy wybieramy 1 wartosc
             Pole.Text += "\n" + dane.ID_Stanowiska.ToString() + " " + dane.Nazwa.ToString();


             //Usuwanie Najpierw szukamy goscia do usuwania a potem go usuwamy
             wyszukaj = db.Stanowisko.Where(c => c.ID_Stanowiska == 4); // szukamy w encji stanowiska o nr4
             dane = wyszukaj.FirstOrDefault<Stanowisko>(); //z wynikow jakie mamy wybieramy 1 wartosc
             db.Stanowisko.Remove(dane);
             db.SaveChanges();

             //Ponownie wyswietlimy zmiany
             wyszukaj = db.Stanowisko.Where(c => c.ID_Stanowiska == 4); // szukamy w encji stanowiska o nr4
             dane = wyszukaj.FirstOrDefault<Stanowisko>(); //z wynikow jakie mamy wybieramy 1 wartosc
             if (dane == null) Pole.Text += "\nBrak danych"; // _dane zwracaja wartosc null jesli nie ma tej osoby

         }*/

