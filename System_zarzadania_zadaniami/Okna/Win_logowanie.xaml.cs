﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace System_zarzadania_zadaniami.Okna
{
    /// <summary>
    /// Interaction logic for Win_logowanie.xaml
    /// </summary>
    public partial class Win_logowanie : Window
    {
        public Win_logowanie()
        {
            InitializeComponent();
            loginBox.Focus();
        }

        public static int IdPracownika;

        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
            using (var db = new BazaEntities())
            {
               
                var ID = Decimal.Parse(loginBox.Text);
                var wyszukaj = from p in db.Pracownicy where p.ID_pracownika == ID select p;//LINQ
                var dane = wyszukaj.FirstOrDefault<Pracownicy>();
                
                if (dane != null)
                {
                    
                    if (dane.haslo == passwordBox.Password)
                    {
                        IdPracownika = Decimal.ToInt32(dane.ID_pracownika);
                        if (dane.Stanowisko.ID_Stanowiska == 1)
                        {
                            var win = new MainWindow(); //Pracownik
                            win.Show();
                            this.Close();
                        }
                        if (dane.Stanowisko.ID_Stanowiska == 2)
                        {
                            var win = new MainWindow2(); //Kierownik
                            win.Show();
                            this.Close();
                        }
                        if (dane.Stanowisko.ID_Stanowiska == 3)
                        {
                            var win = new MainWindow3(); //Administrator
                            win.Show();
                            this.Close();
                        }

                    }
                    else
                    {
                        MessageBox.Show("Wprowadzono niepoprawne dane logowania, spróbuj ponownie!");
                    }
                }
                else
                {
                    MessageBox.Show("Wprowadzono niepoprawne dane logowania, spróbuj ponownie!");
                }
                passwordBox.SelectAll();
            }
        }

        private void registerButton_Click(object sender, RoutedEventArgs e)
        {
            var window = new Win_rejestracja(); //Administrator
            window.Show();
        }
    }
}