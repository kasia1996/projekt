﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace System_zarzadania_zadaniami.Okna
{
    /// <summary>
    /// Interaction logic for Win_powiadomienie.xaml
    /// </summary>
    public partial class Win_powiadomienie : Window
    {
        Powiadomienia powiado = new Powiadomienia();

        public Win_powiadomienie()
        {
            InitializeComponent();
        }

        public Win_powiadomienie(Powiadomienia powiado)
        {
            InitializeComponent();
            this.powiado = powiado;
            txtOpis.Text = powiado.Opis;
            using (var db = new BazaEntities())
            {
                var wyszukaj = from p in db.Powiadomienia
                               where (p.ID_powiadomienia == powiado.ID_powiadomienia)
                               select p;
                powiado = wyszukaj.FirstOrDefault();
            }
                OK_butt.Focus();
        }

        private void OK_butt_Click(object sender, RoutedEventArgs e)
        {
            //Usuwanie powiadomienia z bazy
            using (var db = new BazaEntities())
            {
                db.Powiadomienia.Attach(this.powiado);
                db.Powiadomienia.Remove(this.powiado);
                db.Entry(this.powiado).State = System.Data.EntityState.Deleted;
                db.SaveChanges();
            }
            this.Close();
        }
    }
}
