//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace System_zarzadania_zadaniami
{
    using System;
    using System.Collections.Generic;
    
    public partial class Typ_Notatki
    {
        public Typ_Notatki()
        {
            this.Notatka = new HashSet<Notatka>();
        }
    
        public decimal ID_typ_notatki { get; set; }
        public string nazwa { get; set; }
    
        public virtual ICollection<Notatka> Notatka { get; set; }
    }
}
