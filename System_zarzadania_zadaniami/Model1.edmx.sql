
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/12/2016 21:06:27
-- Generated from EDMX file: C:\Users\Krolik\Source\Repos\l04-g1-system-do-zarzadzania-zadaniami\System_zarzadania_zadaniami\System_zarzadania_zadaniami\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Projekt];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Czat_Pracownicy_FK]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Czat] DROP CONSTRAINT [FK_Czat_Pracownicy_FK];
GO
IF OBJECT_ID(N'[dbo].[FK_Czat_Zadanie_FK]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Czat] DROP CONSTRAINT [FK_Czat_Zadanie_FK];
GO
IF OBJECT_ID(N'[dbo].[FK_Lista_Zadan_Pracownicy_FK]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Lista_Zadan] DROP CONSTRAINT [FK_Lista_Zadan_Pracownicy_FK];
GO
IF OBJECT_ID(N'[dbo].[FK_Lista_Zadan_Zadanie_FK]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Lista_Zadan] DROP CONSTRAINT [FK_Lista_Zadan_Zadanie_FK];
GO
IF OBJECT_ID(N'[dbo].[FK_Notatka_Pracownicy_FK]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Notatka] DROP CONSTRAINT [FK_Notatka_Pracownicy_FK];
GO
IF OBJECT_ID(N'[dbo].[FK_Notatka_Typ_Notatki_FK]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Notatka] DROP CONSTRAINT [FK_Notatka_Typ_Notatki_FK];
GO
IF OBJECT_ID(N'[dbo].[FK_Notatka_Zadanie_FK]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Notatka] DROP CONSTRAINT [FK_Notatka_Zadanie_FK];
GO
IF OBJECT_ID(N'[dbo].[FK_Oceny_Lista_Zadan_FK]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Oceny] DROP CONSTRAINT [FK_Oceny_Lista_Zadan_FK];
GO
IF OBJECT_ID(N'[dbo].[FK_Oceny_Pracownicy_FK]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Oceny] DROP CONSTRAINT [FK_Oceny_Pracownicy_FK];
GO
IF OBJECT_ID(N'[dbo].[FK_Oceny_Typ_Oceny_FK]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Oceny] DROP CONSTRAINT [FK_Oceny_Typ_Oceny_FK];
GO
IF OBJECT_ID(N'[dbo].[FK_Powiadomienia_Lista_Zadan_FK]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Powiadomienia] DROP CONSTRAINT [FK_Powiadomienia_Lista_Zadan_FK];
GO
IF OBJECT_ID(N'[dbo].[FK_Pracownicy_Pracownicy_FK]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Pracownicy] DROP CONSTRAINT [FK_Pracownicy_Pracownicy_FK];
GO
IF OBJECT_ID(N'[dbo].[FK_Pracownicy_Stanowisko_FK]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Pracownicy] DROP CONSTRAINT [FK_Pracownicy_Stanowisko_FK];
GO
IF OBJECT_ID(N'[dbo].[FK_Raport_Pracownicy_FK]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Raport] DROP CONSTRAINT [FK_Raport_Pracownicy_FK];
GO
IF OBJECT_ID(N'[dbo].[FK_Raport_Zadanie_FK]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Raport] DROP CONSTRAINT [FK_Raport_Zadanie_FK];
GO
IF OBJECT_ID(N'[dbo].[FK_Urlop_Pracownicy_FK]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Urlop] DROP CONSTRAINT [FK_Urlop_Pracownicy_FK];
GO
IF OBJECT_ID(N'[dbo].[FK_Urlop_Typ_Urlopu_FK]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Urlop] DROP CONSTRAINT [FK_Urlop_Typ_Urlopu_FK];
GO
IF OBJECT_ID(N'[dbo].[FK_Zadanie_Pracownicy_FK]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Zadanie] DROP CONSTRAINT [FK_Zadanie_Pracownicy_FK];
GO
IF OBJECT_ID(N'[dbo].[FK_Zadanie_Status_FK]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Zadanie] DROP CONSTRAINT [FK_Zadanie_Status_FK];
GO
IF OBJECT_ID(N'[dbo].[FK_Zadanie_Typ_Zadania_FK]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Zadanie] DROP CONSTRAINT [FK_Zadanie_Typ_Zadania_FK];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Czat]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Czat];
GO
IF OBJECT_ID(N'[dbo].[Lista_Zadan]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Lista_Zadan];
GO
IF OBJECT_ID(N'[dbo].[Notatka]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Notatka];
GO
IF OBJECT_ID(N'[dbo].[Oceny]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Oceny];
GO
IF OBJECT_ID(N'[dbo].[Powiadomienia]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Powiadomienia];
GO
IF OBJECT_ID(N'[dbo].[Pracownicy]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Pracownicy];
GO
IF OBJECT_ID(N'[dbo].[Raport]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Raport];
GO
IF OBJECT_ID(N'[dbo].[Stanowisko]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Stanowisko];
GO
IF OBJECT_ID(N'[dbo].[Status]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Status];
GO
IF OBJECT_ID(N'[dbo].[Typ_Notatki]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Typ_Notatki];
GO
IF OBJECT_ID(N'[dbo].[Typ_Oceny]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Typ_Oceny];
GO
IF OBJECT_ID(N'[dbo].[Typ_Urlopu]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Typ_Urlopu];
GO
IF OBJECT_ID(N'[dbo].[Typ_Zadania]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Typ_Zadania];
GO
IF OBJECT_ID(N'[dbo].[Urlop]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Urlop];
GO
IF OBJECT_ID(N'[dbo].[Zadanie]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Zadanie];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Czat'
CREATE TABLE [dbo].[Czat] (
    [ID_wiadomosci] decimal(28,5)  NOT NULL,
    [Tresc] varchar(255)  NOT NULL,
    [Data_wyslania] datetime  NOT NULL,
    [Zadanie_ID_zadania] decimal(28,7)  NOT NULL,
    [Pracownicy_ID_pracownika] decimal(28,9)  NOT NULL
);
GO

-- Creating table 'Lista_Zadan'
CREATE TABLE [dbo].[Lista_Zadan] (
    [ID_listy] int  NOT NULL,
    [Zegar] time  NULL,
    [Pracownicy_ID_pracownika] decimal(28,9)  NOT NULL,
    [Zadanie_ID_zadania] decimal(28,7)  NOT NULL
);
GO

-- Creating table 'Notatka'
CREATE TABLE [dbo].[Notatka] (
    [ID_notatki] decimal(28,5)  NOT NULL,
    [Opis] varchar(300)  NOT NULL,
    [Zadanie_ID_zadania] decimal(28,7)  NOT NULL,
    [Pracownicy_ID_pracownika] decimal(28,9)  NOT NULL,
    [Typ_Notatki_ID_typ_notatki] decimal(28,5)  NOT NULL
);
GO

-- Creating table 'Oceny'
CREATE TABLE [dbo].[Oceny] (
    [ID_Oceny] decimal(28,5)  NOT NULL,
    [Ocena] decimal(2,2)  NOT NULL,
    [Pracownicy_ID_pracownika] decimal(28,9)  NOT NULL,
    [Lista_Zadan_ID_listy] int  NOT NULL,
    [Typ_Oceny_ID_typu_oceny] decimal(28,5)  NOT NULL,
    [Notka] varchar(100)  NULL
);
GO

-- Creating table 'Powiadomienia'
CREATE TABLE [dbo].[Powiadomienia] (
    [ID_powiadomienia] decimal(28,5)  NOT NULL,
    [Opis] varchar(100)  NOT NULL,
    [Lista_Zadan_ID_listy] int  NOT NULL
);
GO

-- Creating table 'Pracownicy'
CREATE TABLE [dbo].[Pracownicy] (
    [ID_pracownika] decimal(28,9)  NOT NULL,
    [haslo] varchar(36)  NOT NULL,
    [Imie] varchar(20)  NOT NULL,
    [Nazwisko] varchar(40)  NOT NULL,
    [Data_urodzenia] datetime  NOT NULL,
    [Data_zatrudnienia] datetime  NOT NULL,
    [Pracownicy_ID_pracownika] decimal(28,9)  NULL,
    [Stanowisko_ID_Stanowiska] int  NOT NULL
);
GO

-- Creating table 'Raport'
CREATE TABLE [dbo].[Raport] (
    [ID_raportu] decimal(28,5)  NOT NULL,
    [Pracownicy_ID_pracownika] decimal(28,9)  NOT NULL,
    [Zadanie_ID_zadania] decimal(28,7)  NOT NULL,
    [Data_utworzenia] datetime  NOT NULL,
    [Opis] varchar(300)  NULL
);
GO

-- Creating table 'Stanowisko'
CREATE TABLE [dbo].[Stanowisko] (
    [ID_Stanowiska] int  NOT NULL,
    [Nazwa] varchar(30)  NULL
);
GO

-- Creating table 'Status'
CREATE TABLE [dbo].[Status] (
    [ID_statusu] decimal(28,5)  NOT NULL,
    [Nazwa] varchar(30)  NOT NULL
);
GO

-- Creating table 'Typ_Notatki'
CREATE TABLE [dbo].[Typ_Notatki] (
    [ID_typ_notatki] decimal(28,5)  NOT NULL,
    [nazwa] varchar(30)  NOT NULL
);
GO

-- Creating table 'Typ_Oceny'
CREATE TABLE [dbo].[Typ_Oceny] (
    [ID_typu_oceny] decimal(28,5)  NOT NULL,
    [nazwa] varchar(20)  NOT NULL
);
GO

-- Creating table 'Typ_Urlopu'
CREATE TABLE [dbo].[Typ_Urlopu] (
    [ID_typu_urlopu] decimal(28,5)  NOT NULL,
    [nazwa] varchar(20)  NOT NULL
);
GO

-- Creating table 'Typ_Zadania'
CREATE TABLE [dbo].[Typ_Zadania] (
    [ID_typu_zadania] decimal(28,5)  NOT NULL,
    [nazwa] varchar(30)  NOT NULL
);
GO

-- Creating table 'Urlop'
CREATE TABLE [dbo].[Urlop] (
    [ID_Urlopu] decimal(28,5)  NOT NULL,
    [Data_rozpoczecia] datetime  NOT NULL,
    [Data_zakonczenia] datetime  NULL,
    [Pracownicy_ID_pracownika] decimal(28,9)  NOT NULL,
    [Typ_Urlopu_ID_typu_urlopu] decimal(28,5)  NOT NULL,
    [Notka] varchar(100)  NULL
);
GO

-- Creating table 'Zadanie'
CREATE TABLE [dbo].[Zadanie] (
    [ID_zadania] decimal(28,7)  NOT NULL,
    [Opis] varchar(300)  NULL,
    [Priorytet] decimal(28,2)  NOT NULL,
    [Data_Dodania] datetime  NOT NULL,
    [Data_Deadline] datetime  NULL,
    [Pracownicy_ID_pracownika] decimal(28,9)  NOT NULL,
    [Data_zakonczenia] datetime  NULL,
    [Status_ID_statusu] decimal(28,5)  NOT NULL,
    [Typ_Zadania_ID_typu_zadania] decimal(28,5)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ID_wiadomosci] in table 'Czat'
ALTER TABLE [dbo].[Czat]
ADD CONSTRAINT [PK_Czat]
    PRIMARY KEY CLUSTERED ([ID_wiadomosci] ASC);
GO

-- Creating primary key on [ID_listy] in table 'Lista_Zadan'
ALTER TABLE [dbo].[Lista_Zadan]
ADD CONSTRAINT [PK_Lista_Zadan]
    PRIMARY KEY CLUSTERED ([ID_listy] ASC);
GO

-- Creating primary key on [ID_notatki] in table 'Notatka'
ALTER TABLE [dbo].[Notatka]
ADD CONSTRAINT [PK_Notatka]
    PRIMARY KEY CLUSTERED ([ID_notatki] ASC);
GO

-- Creating primary key on [ID_Oceny] in table 'Oceny'
ALTER TABLE [dbo].[Oceny]
ADD CONSTRAINT [PK_Oceny]
    PRIMARY KEY CLUSTERED ([ID_Oceny] ASC);
GO

-- Creating primary key on [ID_powiadomienia] in table 'Powiadomienia'
ALTER TABLE [dbo].[Powiadomienia]
ADD CONSTRAINT [PK_Powiadomienia]
    PRIMARY KEY CLUSTERED ([ID_powiadomienia] ASC);
GO

-- Creating primary key on [ID_pracownika] in table 'Pracownicy'
ALTER TABLE [dbo].[Pracownicy]
ADD CONSTRAINT [PK_Pracownicy]
    PRIMARY KEY CLUSTERED ([ID_pracownika] ASC);
GO

-- Creating primary key on [ID_raportu] in table 'Raport'
ALTER TABLE [dbo].[Raport]
ADD CONSTRAINT [PK_Raport]
    PRIMARY KEY CLUSTERED ([ID_raportu] ASC);
GO

-- Creating primary key on [ID_Stanowiska] in table 'Stanowisko'
ALTER TABLE [dbo].[Stanowisko]
ADD CONSTRAINT [PK_Stanowisko]
    PRIMARY KEY CLUSTERED ([ID_Stanowiska] ASC);
GO

-- Creating primary key on [ID_statusu] in table 'Status'
ALTER TABLE [dbo].[Status]
ADD CONSTRAINT [PK_Status]
    PRIMARY KEY CLUSTERED ([ID_statusu] ASC);
GO

-- Creating primary key on [ID_typ_notatki] in table 'Typ_Notatki'
ALTER TABLE [dbo].[Typ_Notatki]
ADD CONSTRAINT [PK_Typ_Notatki]
    PRIMARY KEY CLUSTERED ([ID_typ_notatki] ASC);
GO

-- Creating primary key on [ID_typu_oceny] in table 'Typ_Oceny'
ALTER TABLE [dbo].[Typ_Oceny]
ADD CONSTRAINT [PK_Typ_Oceny]
    PRIMARY KEY CLUSTERED ([ID_typu_oceny] ASC);
GO

-- Creating primary key on [ID_typu_urlopu] in table 'Typ_Urlopu'
ALTER TABLE [dbo].[Typ_Urlopu]
ADD CONSTRAINT [PK_Typ_Urlopu]
    PRIMARY KEY CLUSTERED ([ID_typu_urlopu] ASC);
GO

-- Creating primary key on [ID_typu_zadania] in table 'Typ_Zadania'
ALTER TABLE [dbo].[Typ_Zadania]
ADD CONSTRAINT [PK_Typ_Zadania]
    PRIMARY KEY CLUSTERED ([ID_typu_zadania] ASC);
GO

-- Creating primary key on [ID_Urlopu] in table 'Urlop'
ALTER TABLE [dbo].[Urlop]
ADD CONSTRAINT [PK_Urlop]
    PRIMARY KEY CLUSTERED ([ID_Urlopu] ASC);
GO

-- Creating primary key on [ID_zadania] in table 'Zadanie'
ALTER TABLE [dbo].[Zadanie]
ADD CONSTRAINT [PK_Zadanie]
    PRIMARY KEY CLUSTERED ([ID_zadania] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Pracownicy_ID_pracownika] in table 'Czat'
ALTER TABLE [dbo].[Czat]
ADD CONSTRAINT [FK_Czat_Pracownicy_FK]
    FOREIGN KEY ([Pracownicy_ID_pracownika])
    REFERENCES [dbo].[Pracownicy]
        ([ID_pracownika])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Czat_Pracownicy_FK'
CREATE INDEX [IX_FK_Czat_Pracownicy_FK]
ON [dbo].[Czat]
    ([Pracownicy_ID_pracownika]);
GO

-- Creating foreign key on [Zadanie_ID_zadania] in table 'Czat'
ALTER TABLE [dbo].[Czat]
ADD CONSTRAINT [FK_Czat_Zadanie_FK]
    FOREIGN KEY ([Zadanie_ID_zadania])
    REFERENCES [dbo].[Zadanie]
        ([ID_zadania])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Czat_Zadanie_FK'
CREATE INDEX [IX_FK_Czat_Zadanie_FK]
ON [dbo].[Czat]
    ([Zadanie_ID_zadania]);
GO

-- Creating foreign key on [Pracownicy_ID_pracownika] in table 'Lista_Zadan'
ALTER TABLE [dbo].[Lista_Zadan]
ADD CONSTRAINT [FK_Lista_Zadan_Pracownicy_FK]
    FOREIGN KEY ([Pracownicy_ID_pracownika])
    REFERENCES [dbo].[Pracownicy]
        ([ID_pracownika])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Lista_Zadan_Pracownicy_FK'
CREATE INDEX [IX_FK_Lista_Zadan_Pracownicy_FK]
ON [dbo].[Lista_Zadan]
    ([Pracownicy_ID_pracownika]);
GO

-- Creating foreign key on [Zadanie_ID_zadania] in table 'Lista_Zadan'
ALTER TABLE [dbo].[Lista_Zadan]
ADD CONSTRAINT [FK_Lista_Zadan_Zadanie_FK]
    FOREIGN KEY ([Zadanie_ID_zadania])
    REFERENCES [dbo].[Zadanie]
        ([ID_zadania])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Lista_Zadan_Zadanie_FK'
CREATE INDEX [IX_FK_Lista_Zadan_Zadanie_FK]
ON [dbo].[Lista_Zadan]
    ([Zadanie_ID_zadania]);
GO

-- Creating foreign key on [Lista_Zadan_ID_listy] in table 'Oceny'
ALTER TABLE [dbo].[Oceny]
ADD CONSTRAINT [FK_Oceny_Lista_Zadan_FK]
    FOREIGN KEY ([Lista_Zadan_ID_listy])
    REFERENCES [dbo].[Lista_Zadan]
        ([ID_listy])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Oceny_Lista_Zadan_FK'
CREATE INDEX [IX_FK_Oceny_Lista_Zadan_FK]
ON [dbo].[Oceny]
    ([Lista_Zadan_ID_listy]);
GO

-- Creating foreign key on [Lista_Zadan_ID_listy] in table 'Powiadomienia'
ALTER TABLE [dbo].[Powiadomienia]
ADD CONSTRAINT [FK_Powiadomienia_Lista_Zadan_FK]
    FOREIGN KEY ([Lista_Zadan_ID_listy])
    REFERENCES [dbo].[Lista_Zadan]
        ([ID_listy])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Powiadomienia_Lista_Zadan_FK'
CREATE INDEX [IX_FK_Powiadomienia_Lista_Zadan_FK]
ON [dbo].[Powiadomienia]
    ([Lista_Zadan_ID_listy]);
GO

-- Creating foreign key on [Pracownicy_ID_pracownika] in table 'Notatka'
ALTER TABLE [dbo].[Notatka]
ADD CONSTRAINT [FK_Notatka_Pracownicy_FK]
    FOREIGN KEY ([Pracownicy_ID_pracownika])
    REFERENCES [dbo].[Pracownicy]
        ([ID_pracownika])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Notatka_Pracownicy_FK'
CREATE INDEX [IX_FK_Notatka_Pracownicy_FK]
ON [dbo].[Notatka]
    ([Pracownicy_ID_pracownika]);
GO

-- Creating foreign key on [Typ_Notatki_ID_typ_notatki] in table 'Notatka'
ALTER TABLE [dbo].[Notatka]
ADD CONSTRAINT [FK_Notatka_Typ_Notatki_FK]
    FOREIGN KEY ([Typ_Notatki_ID_typ_notatki])
    REFERENCES [dbo].[Typ_Notatki]
        ([ID_typ_notatki])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Notatka_Typ_Notatki_FK'
CREATE INDEX [IX_FK_Notatka_Typ_Notatki_FK]
ON [dbo].[Notatka]
    ([Typ_Notatki_ID_typ_notatki]);
GO

-- Creating foreign key on [Zadanie_ID_zadania] in table 'Notatka'
ALTER TABLE [dbo].[Notatka]
ADD CONSTRAINT [FK_Notatka_Zadanie_FK]
    FOREIGN KEY ([Zadanie_ID_zadania])
    REFERENCES [dbo].[Zadanie]
        ([ID_zadania])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Notatka_Zadanie_FK'
CREATE INDEX [IX_FK_Notatka_Zadanie_FK]
ON [dbo].[Notatka]
    ([Zadanie_ID_zadania]);
GO

-- Creating foreign key on [Pracownicy_ID_pracownika] in table 'Oceny'
ALTER TABLE [dbo].[Oceny]
ADD CONSTRAINT [FK_Oceny_Pracownicy_FK]
    FOREIGN KEY ([Pracownicy_ID_pracownika])
    REFERENCES [dbo].[Pracownicy]
        ([ID_pracownika])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Oceny_Pracownicy_FK'
CREATE INDEX [IX_FK_Oceny_Pracownicy_FK]
ON [dbo].[Oceny]
    ([Pracownicy_ID_pracownika]);
GO

-- Creating foreign key on [Typ_Oceny_ID_typu_oceny] in table 'Oceny'
ALTER TABLE [dbo].[Oceny]
ADD CONSTRAINT [FK_Oceny_Typ_Oceny_FK]
    FOREIGN KEY ([Typ_Oceny_ID_typu_oceny])
    REFERENCES [dbo].[Typ_Oceny]
        ([ID_typu_oceny])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Oceny_Typ_Oceny_FK'
CREATE INDEX [IX_FK_Oceny_Typ_Oceny_FK]
ON [dbo].[Oceny]
    ([Typ_Oceny_ID_typu_oceny]);
GO

-- Creating foreign key on [Pracownicy_ID_pracownika] in table 'Pracownicy'
ALTER TABLE [dbo].[Pracownicy]
ADD CONSTRAINT [FK_Pracownicy_Pracownicy_FK]
    FOREIGN KEY ([Pracownicy_ID_pracownika])
    REFERENCES [dbo].[Pracownicy]
        ([ID_pracownika])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Pracownicy_Pracownicy_FK'
CREATE INDEX [IX_FK_Pracownicy_Pracownicy_FK]
ON [dbo].[Pracownicy]
    ([Pracownicy_ID_pracownika]);
GO

-- Creating foreign key on [Stanowisko_ID_Stanowiska] in table 'Pracownicy'
ALTER TABLE [dbo].[Pracownicy]
ADD CONSTRAINT [FK_Pracownicy_Stanowisko_FK]
    FOREIGN KEY ([Stanowisko_ID_Stanowiska])
    REFERENCES [dbo].[Stanowisko]
        ([ID_Stanowiska])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Pracownicy_Stanowisko_FK'
CREATE INDEX [IX_FK_Pracownicy_Stanowisko_FK]
ON [dbo].[Pracownicy]
    ([Stanowisko_ID_Stanowiska]);
GO

-- Creating foreign key on [Pracownicy_ID_pracownika] in table 'Raport'
ALTER TABLE [dbo].[Raport]
ADD CONSTRAINT [FK_Raport_Pracownicy_FK]
    FOREIGN KEY ([Pracownicy_ID_pracownika])
    REFERENCES [dbo].[Pracownicy]
        ([ID_pracownika])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Raport_Pracownicy_FK'
CREATE INDEX [IX_FK_Raport_Pracownicy_FK]
ON [dbo].[Raport]
    ([Pracownicy_ID_pracownika]);
GO

-- Creating foreign key on [Pracownicy_ID_pracownika] in table 'Urlop'
ALTER TABLE [dbo].[Urlop]
ADD CONSTRAINT [FK_Urlop_Pracownicy_FK]
    FOREIGN KEY ([Pracownicy_ID_pracownika])
    REFERENCES [dbo].[Pracownicy]
        ([ID_pracownika])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Urlop_Pracownicy_FK'
CREATE INDEX [IX_FK_Urlop_Pracownicy_FK]
ON [dbo].[Urlop]
    ([Pracownicy_ID_pracownika]);
GO

-- Creating foreign key on [Pracownicy_ID_pracownika] in table 'Zadanie'
ALTER TABLE [dbo].[Zadanie]
ADD CONSTRAINT [FK_Zadanie_Pracownicy_FK]
    FOREIGN KEY ([Pracownicy_ID_pracownika])
    REFERENCES [dbo].[Pracownicy]
        ([ID_pracownika])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Zadanie_Pracownicy_FK'
CREATE INDEX [IX_FK_Zadanie_Pracownicy_FK]
ON [dbo].[Zadanie]
    ([Pracownicy_ID_pracownika]);
GO

-- Creating foreign key on [Zadanie_ID_zadania] in table 'Raport'
ALTER TABLE [dbo].[Raport]
ADD CONSTRAINT [FK_Raport_Zadanie_FK]
    FOREIGN KEY ([Zadanie_ID_zadania])
    REFERENCES [dbo].[Zadanie]
        ([ID_zadania])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Raport_Zadanie_FK'
CREATE INDEX [IX_FK_Raport_Zadanie_FK]
ON [dbo].[Raport]
    ([Zadanie_ID_zadania]);
GO

-- Creating foreign key on [Status_ID_statusu] in table 'Zadanie'
ALTER TABLE [dbo].[Zadanie]
ADD CONSTRAINT [FK_Zadanie_Status_FK]
    FOREIGN KEY ([Status_ID_statusu])
    REFERENCES [dbo].[Status]
        ([ID_statusu])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Zadanie_Status_FK'
CREATE INDEX [IX_FK_Zadanie_Status_FK]
ON [dbo].[Zadanie]
    ([Status_ID_statusu]);
GO

-- Creating foreign key on [Typ_Urlopu_ID_typu_urlopu] in table 'Urlop'
ALTER TABLE [dbo].[Urlop]
ADD CONSTRAINT [FK_Urlop_Typ_Urlopu_FK]
    FOREIGN KEY ([Typ_Urlopu_ID_typu_urlopu])
    REFERENCES [dbo].[Typ_Urlopu]
        ([ID_typu_urlopu])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Urlop_Typ_Urlopu_FK'
CREATE INDEX [IX_FK_Urlop_Typ_Urlopu_FK]
ON [dbo].[Urlop]
    ([Typ_Urlopu_ID_typu_urlopu]);
GO

-- Creating foreign key on [Typ_Zadania_ID_typu_zadania] in table 'Zadanie'
ALTER TABLE [dbo].[Zadanie]
ADD CONSTRAINT [FK_Zadanie_Typ_Zadania_FK]
    FOREIGN KEY ([Typ_Zadania_ID_typu_zadania])
    REFERENCES [dbo].[Typ_Zadania]
        ([ID_typu_zadania])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Zadanie_Typ_Zadania_FK'
CREATE INDEX [IX_FK_Zadanie_Typ_Zadania_FK]
ON [dbo].[Zadanie]
    ([Typ_Zadania_ID_typu_zadania]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------